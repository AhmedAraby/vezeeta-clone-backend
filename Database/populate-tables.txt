-- insert doctor into doctor table
insert into doctor values 
(1, "aly","aly@gmail.com", 
"+2012131321", "dentist", 
"ain shames", 50, "");

-- insert client 
insert into client 
values (1, "ahmed", "ahmedaraby605@gmail.com", 01068482084);

-- insert clinic
insert into clinic values 
(1, "shinny smile", "cairo", "down town", 1);

-- insert day schedule 
insert into clinic_schedule values 
(1, "saturday", "08:00:00", "16:00:00", 20, 1);

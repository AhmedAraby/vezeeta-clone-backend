select * from area;
insert into area values(1,"cairo");
insert into area values(2, "giza");
insert into area values(3, "alex");
select * from speciality;
insert into speciality values(1,"dentist");
insert into speciality values(2, "surgon");
insert into speciality values(3, "baby_issue");

select * from client;
insert into client values(
1, "ahmed", "ahmedaraby@605@gmail.com", "01068482084", "1", true, "	", false);
insert into client values(
2, "ahmed", "aaraby@sumerge.com", "01068482084", "1", true, "ROLE_CLIENT", false);
insert into client values(
3, "mohamed", "mohamed@gmail", "01068482084", "2", true, "ROLE_CLIENT", false);

select * from doctor;
insert into doctor values(
1, "abdo", "abdo@gmail.com", "0589461378", "dentist", "MTI", 500,
"https://img.freepik.com/free-vector/doctor-character-background_1270-84.jpg?size=338&ext=jpg",
"1", true, "ROLE_DOCTOR", false
);

insert into doctor values(
2, "saied", "saied@gmail.com", "0589461378", "dentist", "MTI", 200,
"https://c8.alamy.com/comp/2AHBG54/cartoon-male-doctor-holding-a-clipboard-2AHBG54.jpg",
"1", true, "ROLE_DOCTOR", false
);

insert into doctor values(
3, "aly", "aly@gmail.com", "0589461378", "dentist", "MTI", 300,
"https://image.shutterstock.com/image-vector/doctor-vector-illustration-260nw-512904655.jpg",
"1", true, "ROLE_DOCTOR", false
);

insert into doctor values(
4, "hassan", "hassan@gmail.com", "0589461378", "dentist", "MTI", 300,
"https://thumbs.dreamstime.com/b/doctor-cartoon-character-holding-clip-board-pointing-92444463.jpg",
"1", true, "ROLE_DOCTOR", false
);

insert into doctor values(
5, "mohamed", "aly@gmail.com", "0589461378", "surgon", "MTI", 300,
"https://images.assetsdelivery.com/compings_v2/indomercy/indomercy1501/indomercy150100019.jpg",
"1", true, "ROLE_DOCTOR", false
);

select * from clinic;

insert into clinic values(
1, "clinic #1", "cairo", "down town", 1,
"https://image.shutterstock.com/image-photo/hospital-interior-operating-surgery-table-260nw-1407429638.jpg"
);

insert into clinic values(
2, "clinic #2", "cairo", "abbasia", 1,
"https://advice.aqarmap.com.eg/en/wp-content/uploads/2020/01/Clinic.jpg"
);

insert into clinic values(
3, "clinic #3", "cairo", "attaba", 1,
"https://retailinsider.b-cdn.net/wp-content/uploads/2020/08/the-health-clinic-shoppers-exterior.jpg"
);

insert into clinic values(
4, "clinic #1", "cairo", "down town", 2,
"https://image.shutterstock.com/image-photo/hospital-interior-operating-surgery-table-260nw-1407429638.jpg"
);

insert into clinic values(
5, "clinic #2", "cairo", "abbasia", 4,
"https://advice.aqarmap.com.eg/en/wp-content/uploads/2020/01/Clinic.jpg"
);

insert into clinic values(
6, "clinic #3", "cairo", "attaba", 2,
"https://retailinsider.b-cdn.net/wp-content/uploads/2020/08/the-health-clinic-shoppers-exterior.jpg"
);



insert into clinic values(
7, "clinic #1", "giza", "down town", 1,
"https://image.shutterstock.com/image-photo/hospital-interior-operating-surgery-table-260nw-1407429638.jpg"
);

insert into clinic values(
8, "clinic #2", "giza", "abbasia", 1,
"https://advice.aqarmap.com.eg/en/wp-content/uploads/2020/01/Clinic.jpg"
);

insert into clinic values(
9, "clinic #3", "giza", "attaba", 1,
"https://retailinsider.b-cdn.net/wp-content/uploads/2020/08/the-health-clinic-shoppers-exterior.jpg"
);

insert into clinic values(
10, "clinic #1", "cairo", "down town", 1,
"https://image.shutterstock.com/image-photo/hospital-interior-operating-surgery-table-260nw-1407429638.jpg"
);

insert into clinic values(
11, "clinic #2", "giza", "abbasia", 1,
"https://advice.aqarmap.com.eg/en/wp-content/uploads/2020/01/Clinic.jpg"
);

insert into clinic values(
12, "clinic #3", "giza", "attaba", 1,
"https://retailinsider.b-cdn.net/wp-content/uploads/2020/08/the-health-clinic-shoppers-exterior.jpg"
);


# clinic schedule doctor id 1, clinic id = 1;

select * from clinic_schedule;
select * from clinic;

insert into clinic_schedule values(
1, "monday", "08:00:00" , "16:00:00", 20, 1
);

insert into clinic_schedule values(
2, "tuesday", "08:00:00" , "14:00:00", 20, 1
);


insert into clinic_schedule values(
3, "wednesday", "08:00:00" , "13:00:00", 20, 1
);

insert into clinic_schedule values(
4, "monday", "08:00:00" , "16:00:00", 20, 2
);

insert into clinic_schedule values(
5, "tuesday", "08:00:00" , "14:00:00", 20, 2
);


insert into clinic_schedule values(
6, "wednesday", "08:00:00" , "13:00:00", 20, 2
);



insert into clinic_schedule values(
7, "monday", "08:00:00" , "16:00:00", 20, 5
);

insert into clinic_schedule values(
8, "tuesday", "08:00:00" , "14:00:00", 20, 5
);


insert into clinic_schedule values(
9, "wednesday", "08:00:00" , "13:00:00", 20, 5
);

insert into clinic_schedule values(
10, "monday", "08:00:00" , "16:00:00", 20, 5
);

insert into clinic_schedule values(
11, "tuesday", "08:00:00" , "14:00:00", 20, 5
);


insert into clinic_schedule values(
12, "wednesday", "08:00:00" , "13:00:00", 20, 5
);



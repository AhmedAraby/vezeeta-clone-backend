package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.ClientDaoImpl;
import com.vezeeta.clone.backend.dao.ClientTokenDaoImpl;
import com.vezeeta.clone.backend.entities.Client;
import com.vezeeta.clone.backend.entities.ClientToken;
import com.vezeeta.clone.backend.exceptions.EmailAlreadyExistException;
import com.vezeeta.clone.backend.exceptions.InvalidTokenException;
import com.vezeeta.clone.backend.exceptions.TokenExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
public class ClientAuthenticationService {

    @Autowired
    private ClientDaoImpl clientDaoImpl;

    @Autowired
    private ClientTokenDaoImpl clientTokenDaoImpl;

    @Autowired
    private PasswordEncoder passwordEncoder; // bCryptPasswordEncoder

    @Transactional
    public Client SignUp(Client client) throws Exception {
        // encrypt the password
        String encryptedPassword = this.passwordEncoder.encode(client.getPassword());
        client.setPassword(encryptedPassword);



        // make sure the client is not exist before
        boolean exist = false;
        try {
            // exception will be thrown if email is not exist
            Client oldClient = clientDaoImpl.getByEmail(client.getEmail());

            if(oldClient.isEnabled())
                throw new EmailAlreadyExistException("email already exist, use different email", 1);
            else{
                exist = true;
                client = oldClient; // preserve old client data
            }
        }
        catch(EmptyResultDataAccessException exc){ } // client-email do not exist (valid state).

        // this step to avoid the need to confirm the email
        client.setEnabled(true);

        //
        client.setRoles("ROLE_CLIENT");
        clientDaoImpl.save(client); // in case of old client, no new DB request will be sent
        ClientToken clientToken = generateToken(client);
        clientTokenDaoImpl.save(clientToken);
        sendConfirmationMail(
                client.getEmail(),
                getEmailContent(client.getName(), client.getEmail()));

        if(exist) // exist but need to be confirmed
            throw new EmailAlreadyExistException("confirmation email is sent, please confirm you mail", 2);

        return client;
    }

    private ClientToken generateToken(Client client){
        String token = "randomstring-" + LocalDateTime.now();
        ClientToken clientToken = new ClientToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(1),
                client);
        return clientToken;
    }
    private String getEmailContent(String userName, String email){
        return "";
    }

    private String sendConfirmationMail(String toEmail, String content){
        return "";
    }

    @Transactional
    public void confirmToken(String token) {
        ClientToken clientToken;
        try {clientToken = clientTokenDaoImpl.getByToken(token);}
        catch(EmptyResultDataAccessException exc){throw new InvalidTokenException("invalid token");}

        if(clientToken.getConfirmedAt() !=null ||
                clientToken.getExpiresAt().isBefore(LocalDateTime.now()))
            throw new TokenExpiredException("token expired");

        Client client = clientToken.getClient();
        clientTokenDaoImpl.confirm(clientToken);
        clientDaoImpl.enable(client);
    }
}

package com.vezeeta.clone.backend.services;


import com.vezeeta.clone.backend.dao.doctor.resource.DoctorDao;
import com.vezeeta.clone.backend.dao.doctor.resource.DoctorDaoImpl;
import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.DoctorSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DoctorResourceService {
    @Autowired
    DoctorDao doctorDaoImp;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Doctor getDoctor(int id){
        return this.doctorDaoImp.get(id);
    }

    @Transactional
    public List<Doctor> getDoctors(
            DoctorSearchParamsModel doctorSearchParamsModel,
            PaginationInfoModel paginationInfoModel)
    {
        List<Doctor> res = this.doctorDaoImp.getBySearchParams(
                doctorSearchParamsModel,
                paginationInfoModel);
        return res;
    }

    @Transactional
    public void update(Doctor doctor)
    {
        // get original doctor to fill the missed data
        Doctor originalDoctor = this.getDoctor(doctor.getId());
        doctor.setRoles(originalDoctor.getRoles());
        doctor.setEnabled(originalDoctor.isEnabled());

        if(passwordEncoder.matches(doctor.getPassword(), originalDoctor.getPassword()) == false)
            throw new AccessDeniedException("Wrong Password");

        // choose between the old password and the new password
        if(doctor.getNewPassword().equals("") == false)
            doctor.setPassword(doctor.getNewPassword());

        String encryptedPassword = passwordEncoder.encode(doctor.getPassword());
        doctor.setPassword(encryptedPassword);

        System.out.println("doctor to be updated : " + doctor);

        this.doctorDaoImp.merge(doctor);
        return;
    }
}

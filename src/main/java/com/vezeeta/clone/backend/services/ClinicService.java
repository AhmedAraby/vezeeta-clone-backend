package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.clinic.ClinicDao;
import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.entities.Reservation;
import com.vezeeta.clone.backend.exceptions.InvalidUserInputException;
import com.vezeeta.clone.backend.exceptions.ResourceDoNotExistException;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ClinicService {

    @Autowired
    ClinicDao clinicDaoImpl;

    @Transactional
    public Clinic getClinic(int id){
        return this.clinicDaoImpl.getClinic(id);
    }

    @Transactional
    public List<Clinic> getClinics(ClinicSearchParamsModel clinicSearchParams, PaginationInfoModel paginationInfo) {
        List<Clinic> clinics = this.clinicDaoImpl.getClinics(clinicSearchParams, paginationInfo);
        return clinics;
    }
    @Transactional
    public void addClinic(Clinic clinic)
    {
        // make sure that the doctor id in the clinic match the doctor id
        // of the registered doctor that made the request.
        this.clinicDaoImpl.add(clinic);
    }

    @Transactional
    public void updateClinic(Clinic clinic)
    {
        // validate doctor id with the authenticated doctor

        // validate clinic belong to doctor
        if(!this.clinicDaoImpl.clinicExist(clinic.getId(), clinic.getDoctorId()))
            throw new ResourceDoNotExistException("clinic do not exist");
        this.clinicDaoImpl.update(clinic);
    }
    @Transactional
    public long getClinicCount(ClinicSearchParamsModel clinicSearchParams)
    {
        return this.clinicDaoImpl.getClinicsCount(clinicSearchParams);
    }

    @Transactional
    public ClinicSchedule getClinicSchedule(int clinicId, String weekDay)
    {
        return this.clinicDaoImpl.getClinicSchedule(clinicId, weekDay);
    }

    @Transactional
    public List<Reservation> getClinicReservations(int clinicId, Date reservationDate)
    {
        return this.clinicDaoImpl.getClinicReservations(clinicId, reservationDate);
    }

}

package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.clinicSchedule.ClinicScheduleDao;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.exceptions.ResourceDoNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.beans.Transient;
import java.util.List;

@Service
public class ClinicScheduleService {

    @Autowired
    private ClinicScheduleDao clinicScheduleDaoImpl;

    // this operation do not need transactional !!???
    public void addClinicSchedule(ClinicSchedule clinicSchedule){
        try {
            this.clinicScheduleDaoImpl.add(clinicSchedule);
        }
        catch (DataIntegrityViolationException exc){ // foreign key or unique ket constraint violation
//            throw new DataIntegrityViolationException(
//                    "invalid data, clinicId do not exist, or clinic has schedule in this day '"
//                    + clinicSchedule.getWeekday()
//                    + "'");
            throw exc;
        }
    }

    // this operation  do not need transactional !!???
    public void deleteClinicSchedule(ClinicSchedule clinicSchedule){
        this.clinicScheduleDaoImpl.delete(clinicSchedule);
    }

    @Transactional
    public void deleteClinicSchedule(int scheduleId){
        int rowAffected = this.clinicScheduleDaoImpl.delete(scheduleId);
        if(rowAffected == 0)
            throw new ResourceDoNotExistException("schedule to be deleted do not exist");
    }
    @Transactional
    public void deleteClinicSchedule(int clinicId, String weekDay){
        int rowAffected = this.clinicScheduleDaoImpl.delete(clinicId, weekDay);
        if(rowAffected == 0)
            throw new ResourceDoNotExistException("schedule to be deleted do not exist");
        return ;
    }

    public List<ClinicSchedule> getClinicSchedulesByWeekDay(int clinicId){
        return this.clinicScheduleDaoImpl.getClinicSchedules(clinicId);
    }

    @Transactional
    public void updateClinicSchedule(ClinicSchedule clinicSchedule)
    {
        /*
         ----- Important -----
         * do we need to make sure that !!????
            1- that clinic has this clinic schedule
            2- and doctor has this clinic

         * [validate theses assumptions]
         * clinicId and clinicSchedule id are set from the code
         * and user has no interaction with them,
         * assuming that we are enabling CORS policy which mean not any one can send us request.
         */

        this.clinicScheduleDaoImpl.update(clinicSchedule);
    }
}

package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.doctor.resource.DoctorDaoImpl;
import com.vezeeta.clone.backend.dao.DoctorTokenDaoImpl;
import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.entities.DoctorToken;
import com.vezeeta.clone.backend.exceptions.EmailAlreadyExistException;
import com.vezeeta.clone.backend.exceptions.InvalidTokenException;
import com.vezeeta.clone.backend.exceptions.TokenExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
public class DoctorAuthenticationService {

    @Autowired
    private DoctorDaoImpl DoctorDaoImpl;

    @Autowired
    private DoctorTokenDaoImpl DoctorTokenDaoImpl;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Doctor SignUp(Doctor Doctor) throws Exception {
        // encrypt password before saving to the database
        String encryptedPassword = passwordEncoder.encode(Doctor.getPassword());
        Doctor.setPassword(encryptedPassword);

        // make sure the Doctor is not exist before
        boolean exist = false;
        try {
            // exception will be thrown if email is not exist
            Doctor oldDoctor = DoctorDaoImpl.getByEmail(Doctor.getEmail());

            if(oldDoctor.isEnabled())
                throw new EmailAlreadyExistException("email already exist, use different email", 1);
            else{
                exist = true;
                Doctor = oldDoctor; // preserve old Doctor data
            }
        }
        catch(EmptyResultDataAccessException exc){ } // Doctor-email do not exist (valid state).

        // this step to avoid the need to confirm the email
        Doctor.setEnabled(true);

        //
        Doctor.setRoles("ROLE_DOCTOR");
        DoctorDaoImpl.save(Doctor); // in case of old Doctor, no new DB request will be sent
        DoctorToken DoctorToken = generateToken(Doctor);
        DoctorTokenDaoImpl.save(DoctorToken);
        sendConfirmationMail(
                Doctor.getEmail(),
                getEmailContent(Doctor.getName(), Doctor.getEmail()));

        if(exist) // exist but need to be confirmed
            throw new EmailAlreadyExistException("confirmation email is sent, please confirm you mail", 2);

        return Doctor;
    }

    private DoctorToken generateToken(Doctor Doctor){
        String token = "randomstring-" + LocalDateTime.now();
        DoctorToken DoctorToken = new DoctorToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(1),
                Doctor);
        return DoctorToken;
    }
    private String getEmailContent(String userName, String email){
        return "";
    }

    private String sendConfirmationMail(String toEmail, String content){
        return "";
    }

    @Transactional
    public void confirmToken(String token) {
        DoctorToken DoctorToken;
        try {DoctorToken = DoctorTokenDaoImpl.getByToken(token);}
        catch(EmptyResultDataAccessException exc){throw new InvalidTokenException("invalid token");}

        if(DoctorToken.getConfirmedAt() !=null ||
                DoctorToken.getExpiresAt().isBefore(LocalDateTime.now()))
            throw new TokenExpiredException("token expired");

        Doctor Doctor = DoctorToken.getDoctor();
        DoctorTokenDaoImpl.confirm(DoctorToken);
        DoctorDaoImpl.enable(Doctor);
    }
}

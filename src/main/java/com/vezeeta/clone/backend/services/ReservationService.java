package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.reservation.ReservationDao;
import com.vezeeta.clone.backend.entities.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ReservationService {

    @Autowired
    ReservationDao reservationDaoImpl;
    @Transactional
    public Reservation getReservation(int id) {
        return null;
    }

    @Transactional
    public Reservation addReservation(Reservation reservation) {
        return this.reservationDaoImpl.add(reservation);
    }

    @Transactional
    public void deleteReservation() {

    }
}

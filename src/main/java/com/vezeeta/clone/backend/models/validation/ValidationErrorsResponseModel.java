package com.vezeeta.clone.backend.models.validation;

import org.springframework.validation.ObjectError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationErrorsResponseModel {

    private Map<String, String> validationErrors;

    public ValidationErrorsResponseModel() {
        this.validationErrors = new HashMap<>();
    }

    public ValidationErrorsResponseModel(Map<String, String> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public Map<String, String> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(Map<String, String> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public void addError(String field, String message){
        this.validationErrors.put(field, message);
    }

}

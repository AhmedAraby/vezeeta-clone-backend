package com.vezeeta.clone.backend.models.signup;

public class SignupSuccessResponseModel {
    private int id;

    public SignupSuccessResponseModel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SignupSuccessResponseModel{" +
                "id=" + id +
                '}';
    }

}

package com.vezeeta.clone.backend.models;

import javax.validation.constraints.Positive;

public class PaginationInfoModel {
    @Positive
    private int pageNum;
    @Positive
    private int pageSize;

    public PaginationInfoModel() {
    }

    public PaginationInfoModel(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PaginationInfoModel{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                '}';
    }
}

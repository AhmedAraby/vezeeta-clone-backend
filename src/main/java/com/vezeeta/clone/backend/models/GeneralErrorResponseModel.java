/**
 * standard error format for all the Rest End points
 * in this application.
 */

package com.vezeeta.clone.backend.models;

public class GeneralErrorResponseModel {
    private String message;
    private int status;

    public GeneralErrorResponseModel(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public GeneralErrorResponseModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GeneralErrorResponseModel{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}

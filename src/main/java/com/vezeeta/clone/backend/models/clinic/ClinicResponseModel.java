package com.vezeeta.clone.backend.models.clinic;

import com.vezeeta.clone.backend.entities.Clinic;

import java.util.List;

public class ClinicResponseModel {
    private long count;
    private List<Clinic> clinics;
    private ClinicLinksModel clinicLinks;

    public ClinicResponseModel() {
    }

    public ClinicResponseModel(long count, List<Clinic> clinics, ClinicLinksModel clinicLinks) {
        this.count = count;
        this.clinics = clinics;
        this.clinicLinks = clinicLinks;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<Clinic> getClinics() {
        return clinics;
    }

    public void setClinics(List<Clinic> clinics) {
        this.clinics = clinics;
    }

    public ClinicLinksModel getClinicLinks() {
        return clinicLinks;
    }

    public void setClinicLinks(ClinicLinksModel clinicLinks) {
        this.clinicLinks = clinicLinks;
    }

    @Override
    public String toString() {
        return "ClinicResponseModel{" +
                "count=" + count +
                ", clinics=" + clinics +
                ", clinicLinks=" + clinicLinks +
                '}';
    }
}

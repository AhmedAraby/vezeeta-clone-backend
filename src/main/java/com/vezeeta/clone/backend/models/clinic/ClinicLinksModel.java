package com.vezeeta.clone.backend.models.clinic;

import com.vezeeta.clone.backend.models.PaginationInfoModel;

public class ClinicLinksModel {
    private final String api = "http://localhost:8050/clinics";
    private String prevLink;
    private String nextLink;

    public ClinicLinksModel() {
    }

    public ClinicLinksModel(ClinicSearchParamsModel clinicSearchParams, PaginationInfoModel paginationInfo, long count) {
        this.buildNextLink(clinicSearchParams, paginationInfo, count);
        this.buildPrevLink(clinicSearchParams, paginationInfo);
    }
    public ClinicLinksModel(String prevLink, String nextLink) {
        this.prevLink = prevLink;
        this.nextLink = nextLink;
    }

    private void buildPrevLink(
            ClinicSearchParamsModel clinicSearchParams,
            PaginationInfoModel paginationInfo)
    {
        if(paginationInfo.getPageNum() == 1) {
            this.prevLink = null;
            return ;
        }
        this.prevLink = String.format(this.api + "?doctorId=%s&area=%s&pageNum=%d&pageSize=%d",
                clinicSearchParams.getDoctorId(),
                clinicSearchParams.getArea(),
                paginationInfo.getPageNum() - 1, paginationInfo.getPageSize());
    }

    private void buildNextLink(
            ClinicSearchParamsModel clinicSearchParams,
            PaginationInfoModel paginationInfo,
            long count)
    {
        if(paginationInfo.getPageNum() * paginationInfo.getPageSize() >= count) {
            this.nextLink = null;
            return ;
        }
        this.nextLink = String.format(this.api + "?doctorId=%s&area=%s&pageNum=%d&pageSize=%d",
                clinicSearchParams.getDoctorId(),
                clinicSearchParams.getArea(),
                paginationInfo.getPageNum() + 1, paginationInfo.getPageSize());
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    @Override
    public String toString() {
        return "ClinicLinksModel{" +
                "prevLink='" + prevLink + '\'' +
                ", nextLink='" + nextLink + '\'' +
                '}';
    }
}

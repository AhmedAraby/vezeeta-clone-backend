package com.vezeeta.clone.backend.models.signup;

public class SignupErrorResponseModel {

    private String message;

    /**
     * 1 = email exist and enabled, user another email
     * 2 = email exist but need to be confirmed, please confirm.
     */
    private int errorCode;

    public SignupErrorResponseModel(String message, int errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }

    public SignupErrorResponseModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "SignupErrorResponseModel{" +
                "message='" + message + '\'' +
                ", errorCode=" + errorCode +
                '}';
    }
}

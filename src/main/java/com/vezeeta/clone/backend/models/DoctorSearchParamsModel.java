package com.vezeeta.clone.backend.models;

public class DoctorSearchParamsModel {
    private String area;
    private String speciality;
    private  String name;

    public DoctorSearchParamsModel() {}

    public DoctorSearchParamsModel(String area, String speciality, String name) {
        this.area = area;
        this.speciality = speciality;
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getName() {
        return name;
    }

    public void setName(String doctorName) {
        this.name = doctorName;
    }

    @Override
    public String toString() {
        return "DoctorSearchParamsModel{" +
                "area='" + area + '\'' +
                ", speciality='" + speciality + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

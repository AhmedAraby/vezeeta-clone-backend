package com.vezeeta.clone.backend.models.clinic;

public class ClinicSearchParamsModel {
    private int doctorId;
    private String area;

    public ClinicSearchParamsModel() {
    }

    public ClinicSearchParamsModel(int doctorId, String area) {
        this.doctorId = doctorId;
        this.area = area;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "ClinicSearchParamsModel{" +
                "doctorId=" + doctorId +
                ", area='" + area + '\'' +
                '}';
    }
}

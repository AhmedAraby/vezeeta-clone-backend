package com.vezeeta.clone.backend.models.login;

/**
 * we will use this class to send the response
 * then it will be intercepted by jkson to be converted into json
 */

public class LoginSuccessResponseModel {

    private int id;
    private String jwt;

    public LoginSuccessResponseModel(){}

    public LoginSuccessResponseModel(int id, String jwt){
        this.id = id;
        this.jwt = jwt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
            this.id = id;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    @Override
    public String toString() {
        return "LoginSuccessResponseModel{" +
                "id=" + id +
                ", jwt='" + jwt + '\'' +
                '}';
    }
}

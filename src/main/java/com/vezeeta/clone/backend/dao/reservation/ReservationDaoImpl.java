package com.vezeeta.clone.backend.dao.reservation;

import com.vezeeta.clone.backend.entities.Reservation;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class ReservationDaoImpl implements ReservationDao{

    @Autowired
    EntityManager entityManager;

    private Session getSession(){
        return this.entityManager.unwrap(Session.class);
    }

    @Override
    public Reservation get(int id) {
        return null;
    }

    @Override
    public Reservation update(Reservation reservation) {
        return null;
    }

    @Override
    public Reservation add(Reservation reservation) {
        Session session = getSession();
        session.save(reservation);
        return reservation;
    }

    @Override
    public void delete() {

    }
}

package com.vezeeta.clone.backend.dao.clinic;

import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.entities.Reservation;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ClinicDaoImpl implements ClinicDao{
    @Autowired
    EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager){
        this.entityManager =  entityManager;  // for testing purpose
    }


    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }

    @Override
    public Clinic getClinic(int id) {
        Session session = this.getSession();
        Clinic clinic = session.get(Clinic.class, id);
        return clinic;
    }

    public String getQueryConditions(ClinicSearchParamsModel clinicSearchParams)
    {
        String condStart = " where ";
        List<String> queryParts = new ArrayList<>();

        if(clinicSearchParams.getDoctorId() > 0){
            queryParts.add(String.format(" %s d.id=:doctorId ",condStart ));
            condStart = " and ";
        }
        if(clinicSearchParams.getArea() != null) {
            queryParts.add(String.format(" %s c.location=:area ", condStart ));
            condStart = " and ";
        }

        return String.join(" ", queryParts);
    }

    public Query setQueryCondsParameters(Query query, ClinicSearchParamsModel clinicSearchParams)
    {

        if(clinicSearchParams.getDoctorId() > 0)
            query.setParameter("doctorId", clinicSearchParams.getDoctorId());

        if(clinicSearchParams.getArea() != null)
            query.setParameter("area", clinicSearchParams.getArea());

        return query;
    }

    @Override
    public List<Clinic> getClinics(ClinicSearchParamsModel clinicSearchParams, PaginationInfoModel paginationInfo) {
        String queryStr = " select c from Clinic c " +
                            " join c.doctor d " +
                            this.getQueryConditions(clinicSearchParams) +
                            " order by c.id ";

        Session session = this.getSession();
        Query<Clinic> clinicQuery = session.createQuery(queryStr, Clinic.class);
        System.out.println("clinic query "  +  queryStr);
        // params
        clinicQuery = this.setQueryCondsParameters(clinicQuery, clinicSearchParams);

        // pagination
        clinicQuery.setFirstResult((paginationInfo.getPageNum() - 1) * paginationInfo.getPageSize());
        clinicQuery.setMaxResults(paginationInfo.getPageSize());

        List<Clinic> clinics = clinicQuery.getResultList();
        return clinics;
    }

    @Override
    public void add(Clinic clinic) {
        Session session = this.getSession();
        session.save(clinic);
    }

    @Override
    public long getClinicsCount(ClinicSearchParamsModel clinicSearchParams)
    {
        String queryStr = " select count(c) from Clinic c " +
                " join c.doctor d " +
                this.getQueryConditions(clinicSearchParams);

        Session session = this.getSession();
        Query<Long> clinicQuery = session.createQuery(queryStr, Long.class);

        this.setQueryCondsParameters(clinicQuery, clinicSearchParams);

        long count = clinicQuery.getSingleResult();
        return count;
    }

    @Override
    public ClinicSchedule getClinicSchedule(int clinicId, String weekDay)
    {
        String queryStr = "select cs from Clinic c " +
                            " join c.clinicSchedules cs " +
                            " where c.id=:clinicId " +
                            "and cs.weekday=:weekday";
        Session session = this.getSession();
        Query<ClinicSchedule> clinicScheduleQuery = session.createQuery(queryStr, ClinicSchedule.class);

        clinicScheduleQuery.setParameter("clinicId", clinicId);
        clinicScheduleQuery.setParameter("weekday", weekDay);

        ClinicSchedule clinicSchedule = clinicScheduleQuery.getSingleResult();
        return clinicSchedule;
    }

    @Override
    public List<Reservation> getClinicReservations(int clinicId, Date reservationDate) {
        String queryStr = "select r from Clinic c " +
                " join c.reservations r " +
                " where c.id=:clinicId " +
                "and r.date=:reservationDate";
        Session session = this.getSession();
        Query<Reservation> clinicReservationsQuery = session.createQuery(queryStr, Reservation.class);

        clinicReservationsQuery.setParameter("clinicId", clinicId);
        clinicReservationsQuery.setParameter("reservationDate", reservationDate);

        List<Reservation> clinicReservations = clinicReservationsQuery.getResultList();
        return clinicReservations;
    }

    @Override
    public boolean clinicExist(int clinicId, int doctorId) {
        String queryStr = " select count(c) from Clinic c " +
                        " where c.id=:clinicId " +
                        " and c.doctor.id=:doctorId ";
        Session session = getSession();
        Query<Long> clinicQuery = session.createQuery(queryStr, Long.class);

        clinicQuery.setParameter("clinicId", clinicId);
        clinicQuery.setParameter("doctorId", doctorId);

        Long count = clinicQuery.getSingleResult();
        return count  == 1;
    }

    @Override
    public void update(Clinic clinic) {
        Session session = getSession();
        session.update(clinic);
    }


    @Override
    public void deleteClinic(Clinic clinic) {

    }

    @Override
    public void deleteClinic(int id) {

    }
}

package com.vezeeta.clone.backend.dao.clinic;


import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.entities.Reservation;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Repository
public interface ClinicDao {
    public Clinic getClinic(int id);
    public List<Clinic> getClinics(ClinicSearchParamsModel clinicSearchParams, PaginationInfoModel paginationInfo);
    public void add(Clinic clinic);
    public long getClinicsCount(ClinicSearchParamsModel clinicSearchParams);
    public ClinicSchedule getClinicSchedule(int clinicId, String weekDay);
    public List<Reservation> getClinicReservations(int clinicId, Date reservationDate);
    public boolean clinicExist(int clinicId, int doctorId);
    public void update(Clinic clinic);
    public void deleteClinic(Clinic clinic);
    public void deleteClinic(int id);
}

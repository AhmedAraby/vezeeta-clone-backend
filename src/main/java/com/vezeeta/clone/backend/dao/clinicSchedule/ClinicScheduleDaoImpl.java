package com.vezeeta.clone.backend.dao.clinicSchedule;

import com.vezeeta.clone.backend.entities.ClinicSchedule;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class ClinicScheduleDaoImpl implements ClinicScheduleDao{

    @Autowired
    EntityManager entityManager;

    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }

    @Override
    public List<ClinicSchedule> getClinicSchedules(int clinicId) {
        String queryStr = " select cs from ClinicSchedule cs " +
                            " where cs.clinic.id=:clinicId ";
        Session session = this.getSession();
        Query<ClinicSchedule> clinicScheduleQuery = session.createQuery(queryStr, ClinicSchedule.class);
        clinicScheduleQuery.setParameter("clinicId", clinicId);

        List<ClinicSchedule> res = clinicScheduleQuery.getResultList();
        return res;
    }

    @Override
    public void add(ClinicSchedule clinicSchedule) {
        Session session = this.getSession();
        session.save(clinicSchedule);
    }

    @Override
    public void delete(ClinicSchedule clinicSchedule) {
        Session session = this.getSession();
        session.delete(clinicSchedule);
    }

    @Override
    public int delete(int scheduleId) {
        String queryStr = " delete from ClinicSchedule cs " +
                            " where cs.id=:scheduleId ";
        Session session = this.getSession();
        Query deleteQuery = session.createQuery(queryStr);

        deleteQuery.setParameter("scheduleId", scheduleId);

        int rowsAffected = deleteQuery.executeUpdate();
        return rowsAffected;    }

    @Override
    public int delete(int clinicId, String weekDay) {
        String queryStr = " delete from ClinicSchedule cs " +
                            " where cs.clinic.id=:clinicId " +
                            " and cs.weekday=:weekDay ";
        Session session = this.getSession();
        Query deleteQuery = session.createQuery(queryStr);

        deleteQuery.setParameter("clinicId", clinicId);
        deleteQuery.setParameter("weekDay", weekDay);

        int rowsAffected = deleteQuery.executeUpdate();
        return rowsAffected;
    }


    @Override
    public void update(ClinicSchedule clinicSchedule) {
        Session session = this.getSession();
        session.update(clinicSchedule);
    }
}

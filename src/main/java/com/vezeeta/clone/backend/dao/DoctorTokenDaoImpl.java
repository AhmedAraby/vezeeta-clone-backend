package com.vezeeta.clone.backend.dao;

import com.vezeeta.clone.backend.entities.DoctorToken;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Repository
public class DoctorTokenDaoImpl {
    @Autowired
    private EntityManager entityManager;
    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }


    public DoctorToken save(DoctorToken DoctorToken){
        Session session = getSession();
        session.save(DoctorToken);
        return DoctorToken;
    }

    public DoctorToken getByToken(String token){
        Session session = getSession();
        Query<DoctorToken> DoctorTokenQ = session.createQuery(
                "from DoctorToken dt where dt.token=:token",
                DoctorToken.class
        );
        DoctorTokenQ.setParameter("token", token);

        return DoctorTokenQ.getSingleResult();
    }

    public DoctorToken confirm(DoctorToken DoctorToken){
        Session session = getSession();
        DoctorToken.setConfirmedAt(LocalDateTime.now());
        session.save(DoctorToken);
        return DoctorToken;
    }
    public DoctorToken resetExpirationDate(DoctorToken DoctorToken){
        Session session = getSession();
        DoctorToken.setExpiresAt(LocalDateTime.now().plusMinutes(1));
        session.save(DoctorToken);
        return DoctorToken;
    }
}

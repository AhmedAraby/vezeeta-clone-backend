package com.vezeeta.clone.backend.dao;

import com.vezeeta.clone.backend.entities.Speciality;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class SpecialityDaoImpl {
    @Autowired
    EntityManager entityManager;

    @Transactional
    public List<Speciality> getSpecialities()
    {
        Session session = entityManager.unwrap(Session.class);
        Query<Speciality> specialityQ = session.createQuery("from Speciality", Speciality.class);
        List<Speciality> res  = specialityQ.getResultList();
        return res;
    }
}

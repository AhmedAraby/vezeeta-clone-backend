package com.vezeeta.clone.backend.dao.clinicSchedule;

import com.vezeeta.clone.backend.entities.ClinicSchedule;

import java.util.List;

public interface ClinicScheduleDao {
    public List<ClinicSchedule> getClinicSchedules(int clinicId);

    public void add(ClinicSchedule clinicSchedule);
    public void delete(ClinicSchedule clinicSchedule);
    public int delete(int scheduleId);
    public int delete(int clinicId, String weekDay);
    public void update(ClinicSchedule clinicSchedule);
}

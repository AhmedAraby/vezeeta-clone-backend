package com.vezeeta.clone.backend.dao.doctor.resource;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.DoctorSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DoctorDaoImpl implements DoctorDao {
    @Autowired
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager){
        this.entityManager =  entityManager;  // for testing purpose
    }

    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }


    public Doctor get(int DoctorId){
        Session session = getSession();
        return session.get(Doctor.class, DoctorId);
    }

    public Doctor getByEmail(String email){
        Session session = getSession();
        Query<Doctor> DoctorQ =
                session.createQuery("from Doctor d where d.email=:email", Doctor.class);
        DoctorQ.setParameter("email", email);
        Doctor Doctor = DoctorQ.getSingleResult();
        return Doctor;
    }

    private String getQueryConditions(DoctorSearchParamsModel doctorSearchParamsModel){
        List<String> queryParts = new ArrayList<>();

        String condStart = " where ";
        if(doctorSearchParamsModel.getArea() != null) {
            queryParts.add(condStart + " c.location=:area ");
            condStart = " and ";
        }
        if(doctorSearchParamsModel.getSpeciality() != null) {
            queryParts.add(condStart + " d.speciality=:speciality ");
            condStart = " and ";
        }
        return String.join(" ", queryParts);
    }

    private Query setQueryCondParameters(
            DoctorSearchParamsModel doctorSearchParamsModel,
            Query query)
    {
        if(doctorSearchParamsModel.getArea() != null)
            query.setParameter("area", doctorSearchParamsModel.getArea());
        if(doctorSearchParamsModel.getSpeciality() != null)
            query.setParameter("speciality", doctorSearchParamsModel.getSpeciality());
        return query;
    }

    public List<Doctor> getBySearchParams(
            DoctorSearchParamsModel doctorSearchParamsModel,
            PaginationInfoModel paginationInfoModel)
    {
        String query = " select distinct d from Doctor d " +
                " join d.clinics as c " +
                getQueryConditions(doctorSearchParamsModel) +    // generate queries dynamically
                " order by d.id ";

        Session session = getSession();
        Query<Doctor> doctorsQuery = session.createQuery(query, Doctor.class);
        doctorsQuery = setQueryCondParameters(doctorSearchParamsModel, doctorsQuery);
        // pagination
        doctorsQuery.setFirstResult((paginationInfoModel.getPageNum() -1) * paginationInfoModel.getPageSize());
        doctorsQuery.setMaxResults(paginationInfoModel.getPageSize());
        List<Doctor> res = doctorsQuery.getResultList();

        return res;
    }

    public Doctor save(Doctor Doctor){
        Session session = getSession();
        session.saveOrUpdate(Doctor);  // if primary key is not 0, update query will be issued
        return Doctor;
    }


    public Doctor add(Doctor Doctor){
        Session session = getSession();
        session.save(Doctor);  // if primary key is not 0, update query will be issued
        return Doctor;
    }

    @Override
    public void update(Doctor doctor) {
        Session session = this.getSession();
        session.update(doctor);
    }

    @Override
    public void merge(Doctor doctor) {
        // avoid the exception of having object with the same identifier of the cache of the session
        Session session = this.getSession();
        session.merge(doctor);
    }

    public Doctor enable(Doctor Doctor){
        Session session = getSession();
        Doctor.setEnabled(true);
        session.save(Doctor);

        return Doctor;
    }

    public void delete(Doctor Doctor){
        Session session = getSession();
        session.delete(Doctor);

    }

}

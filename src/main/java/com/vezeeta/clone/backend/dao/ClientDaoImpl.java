package com.vezeeta.clone.backend.dao;

import com.vezeeta.clone.backend.entities.Client;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class ClientDaoImpl {
    @Autowired
    private EntityManager entityManager;
    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }

    public Client get(int clientId){
        Session session = getSession();
        return session.get(Client.class, clientId);
    }

    public Client getByEmail(String email){
        Session session = getSession();
        Query<Client> clientQ =
                session.createQuery("from Client c where c.email=:email", Client.class);
        clientQ.setParameter("email", email);
        Client client = clientQ.getSingleResult();
        return client;
    }

    public Client save(Client client){
        Session session = getSession();
        session.saveOrUpdate(client);  // if primary key is not 0, update query will be issued
        return client;
    }

    public Client enable(Client client){
        Session session = getSession();
        client.setEnabled(true);
        session.save(client);

        return client;
    }
}

package com.vezeeta.clone.backend.dao;

import com.vezeeta.clone.backend.entities.Client;
import com.vezeeta.clone.backend.entities.ClientToken;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Repository
public class ClientTokenDaoImpl {
    @Autowired
    private EntityManager entityManager;
    private Session getSession(){
        return entityManager.unwrap(Session.class);
    }


    public ClientToken save(ClientToken clientToken){
        Session session = getSession();
        session.save(clientToken);
        return clientToken;
    }

    public ClientToken getByToken(String token){
        Session session = getSession();
        Query<ClientToken> clientTokenQ = session.createQuery(
                "from ClientToken ct where ct.token=:token",
                ClientToken.class
        );
        clientTokenQ.setParameter("token", token);

        return clientTokenQ.getSingleResult();
    }

    public ClientToken confirm(ClientToken clientToken){
        Session session = getSession();
        clientToken.setConfirmedAt(LocalDateTime.now());
        session.save(clientToken);
        return clientToken;
    }
    public ClientToken resetExpirationDate(ClientToken clientToken){
        Session session = getSession();
        clientToken.setExpiresAt(LocalDateTime.now().plusMinutes(1));
        session.save(clientToken);
        return clientToken;
    }
}

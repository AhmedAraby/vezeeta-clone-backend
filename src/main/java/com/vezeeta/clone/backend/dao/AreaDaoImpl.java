package com.vezeeta.clone.backend.dao;

import com.vezeeta.clone.backend.entities.Area;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AreaDaoImpl {

    @Autowired
    EntityManager entityManager;

    @Transactional
    public List<Area> getAreas()
    {
        Session session = entityManager.unwrap(Session.class);
        Query<Area> areaQ = session.createQuery("from Area", Area.class);
        List<Area> res  = areaQ.getResultList();
        return res;
    }
}

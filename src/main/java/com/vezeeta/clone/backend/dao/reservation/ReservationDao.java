package com.vezeeta.clone.backend.dao.reservation;

import com.vezeeta.clone.backend.entities.Reservation;

public interface ReservationDao {
    public Reservation get(int id);
    public Reservation update(Reservation reservation);
    public Reservation add(Reservation reservation);
    public void delete();
}

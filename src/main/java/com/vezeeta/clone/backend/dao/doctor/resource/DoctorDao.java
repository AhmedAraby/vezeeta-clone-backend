package com.vezeeta.clone.backend.dao.doctor.resource;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.DoctorSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;

import java.util.List;

public interface DoctorDao {
    public Doctor get(int DoctorId);
    public Doctor getByEmail(String email);
    public Doctor save(Doctor Doctor);
    public Doctor add(Doctor Doctor);
    public void update(Doctor doctor);
    public void delete(Doctor doctor);
    public void merge(Doctor doctor);
    public Doctor enable(Doctor Doctor);
    List<Doctor> getBySearchParams(DoctorSearchParamsModel doctorSearchParamsModel, PaginationInfoModel paginationInfoModel);
}

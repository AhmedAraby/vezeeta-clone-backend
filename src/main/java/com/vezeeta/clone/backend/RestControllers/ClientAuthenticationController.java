package com.vezeeta.clone.backend.RestControllers;

import com.vezeeta.clone.backend.entities.Client;
import com.vezeeta.clone.backend.models.login.LoginRequestModel;
import com.vezeeta.clone.backend.models.login.LoginSuccessResponseModel;
import com.vezeeta.clone.backend.models.signup.SignupSuccessResponseModel;
import com.vezeeta.clone.backend.security.models.ClientDetails;
import com.vezeeta.clone.backend.security.tokens.ClientUsernamePasswordAuthenticationToken;
import com.vezeeta.clone.backend.security.services.ClientDetailsService;
import com.vezeeta.clone.backend.services.ClientAuthenticationService;
import com.vezeeta.clone.backend.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ClientAuthenticationController {
    @Autowired
    private ClientDetailsService clientDetailsService;
    @Autowired
    private AuthenticationManager authenticationManager; // to lunch the auth process
    @Autowired
    private ClientAuthenticationService clientAuthenticationService;
    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/authenticate/login/client")
    public ResponseEntity<?> clientLogin(@RequestBody LoginRequestModel loginRequest) {
        System.out.println("/authenticate/login/client");

        authenticationManager.authenticate(
                new ClientUsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(), loginRequest.getPassword()));
        ClientDetails clientDetails =
                (ClientDetails) clientDetailsService.loadUserByUsername(loginRequest.getEmail());
        // what is the possible Exceptions that could be thrown by JWT !!???
        Map<String, Object> claims = getClaims(clientDetails);
        String jwt = jwtUtil.generateJWT(claims);

        return ResponseEntity.ok(new LoginSuccessResponseModel(clientDetails.getId(), jwt));
     }

    @PostMapping("/authenticate/signup/client")
    public ResponseEntity<SignupSuccessResponseModel> clientSignUp(@RequestBody Client client) throws Exception {
        System.out.println("client sign up -- " + client);
        client.setId(0);
        client = clientAuthenticationService.SignUp(client);
        return ResponseEntity
                .status(201) // created
                .body(new SignupSuccessResponseModel(client.getId()));
    }

    private Map<String, Object> getClaims(ClientDetails clientDetails){
        Map<String, Object> claims = new HashMap<>();
        claims.put("email", clientDetails.getUsername());
        claims.put("id", clientDetails.getId());
        claims.put("name", clientDetails.getName());
        claims.put("roles", clientDetails.getRoles());

        return claims;
    }

}

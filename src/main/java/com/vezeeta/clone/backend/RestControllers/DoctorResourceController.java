package com.vezeeta.clone.backend.RestControllers;


import com.vezeeta.clone.backend.dao.doctor.resource.DoctorDao;
import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.DoctorSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import com.vezeeta.clone.backend.services.DoctorResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DoctorResourceController {

    @Autowired
    DoctorResourceService doctorResourceService;
    @GetMapping("/doctors/{id}")
    public ResponseEntity<Doctor> getDoctor(@PathVariable int id){
        Doctor doctor = this.doctorResourceService.getDoctor(id);
        return ResponseEntity.ok(doctor);
    }

    @GetMapping("/doctors")
    public ResponseEntity<List<Doctor>> getDoctors(
            DoctorSearchParamsModel doctorSearchParamsModel,
            @Valid PaginationInfoModel paginationInfoModel )
    {
        List<Doctor> res = this.doctorResourceService.getDoctors(doctorSearchParamsModel, paginationInfoModel);
        return ResponseEntity.ok(res);
    }


    @PutMapping("/doctors")
    public ResponseEntity<Void> updateDoctor(@Valid @RequestBody Doctor doctor)
    {
        System.out.println("doctor data is : " + doctor);
        this.doctorResourceService.update(doctor);
        return ResponseEntity.ok(null);
    }
}

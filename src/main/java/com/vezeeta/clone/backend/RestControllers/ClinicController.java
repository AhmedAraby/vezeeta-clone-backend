package com.vezeeta.clone.backend.RestControllers;

import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.entities.Reservation;
import com.vezeeta.clone.backend.exceptions.InvalidUserInputException;
import com.vezeeta.clone.backend.models.clinic.ClinicLinksModel;
import com.vezeeta.clone.backend.models.clinic.ClinicResponseModel;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import com.vezeeta.clone.backend.services.ClinicService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;


import javax.validation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/clinics")
public class ClinicController {

    @Autowired
    ClinicService clinicService;

    @GetMapping("/{id}")
    public ResponseEntity<Clinic> getClinic(@PathVariable("id") int id)
    {
        Clinic clinic = this.clinicService.getClinic(id);
        return ResponseEntity.ok(clinic);
    }

    @GetMapping("")
    public ResponseEntity<ClinicResponseModel> getClinics(
            // theses objects are binded from query variables, is this a good practice !?
            ClinicSearchParamsModel clinicSearchParams,
            @Valid PaginationInfoModel paginationInfo)
    {
        /**
         * null and undefined recived from frontEnd
         * will be "null" and "undefined" which mean that are actual string values
         * and this harm my dynamic query building in the DAO.
         *
         * this is a temporary solution, untill I found  a robust one
         *
         * this could will "FAILL" if we actually want to pass an area name = "null",
         * if there is an area with name we are so unlucky
         */

        System.out.println("params : " +  clinicSearchParams);
        if(clinicSearchParams.getArea() == null){
            System.out.println("area is null");
        }
        if(clinicSearchParams.getArea().equals("null") )
            clinicSearchParams.setArea(null);

        long count = this.clinicService.getClinicCount(clinicSearchParams);

        if((paginationInfo.getPageNum() -1 ) * paginationInfo.getPageSize() >= count)
            return ResponseEntity.status(404).body(null);

        List<Clinic> clinics = this.clinicService.getClinics(clinicSearchParams, paginationInfo);

        // build the response
        ClinicLinksModel clinicLinks = new ClinicLinksModel(clinicSearchParams, paginationInfo, count);
        ClinicResponseModel clinicResponse = new ClinicResponseModel(count, clinics, clinicLinks);
        return ResponseEntity.ok(clinicResponse);
    }

    @PostMapping("")
    public ResponseEntity<Clinic> addClinic(@Valid @RequestBody Clinic clinic)
    {
        clinic.setId(0);
        this.clinicService.addClinic(clinic);
        return ResponseEntity.status(201).body(clinic);
    }

    /**
     * bean validation
     * security validation
     *      clinic belong to the doctor
     *      doctorId attached to the clinic is the same as the authenticated doctor
     * initiate update proces
     */

    @PutMapping("")
    public ResponseEntity<Void> updateClinic(@Valid @RequestBody Clinic clinic)
    {
        this.clinicService.updateClinic(clinic);
        return ResponseEntity.ok(null);
    }

    /**
     * get clinic schedule by
     * clinic id
     * and schedule day
     */
    @GetMapping("/{id}/schedule")
    public ResponseEntity<ClinicSchedule> getClinicSchedule(
            @PathVariable("id") int clinicId,
            @RequestParam("weekday") String weekDay)
    {
        ClinicSchedule clinicSchedule = this.clinicService.getClinicSchedule(clinicId, weekDay);
        return ResponseEntity.ok(clinicSchedule);
    }

    @GetMapping("/{id}/reservations")
    public List<Reservation> getClinicReservations(
            @PathVariable("id") int clinicId,
            @RequestParam("reservationDate") String reservationDate)
    {
        Date rDate = null;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            rDate = df.parse(reservationDate);
        }catch (ParseException exc){
            throw new InvalidUserInputException("Invalid Date Format");
        }
        List<Reservation> clinicReservations =
                this.clinicService.getClinicReservations(clinicId, rDate);
        return clinicReservations;
    }
}

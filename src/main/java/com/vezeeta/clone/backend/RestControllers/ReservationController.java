package com.vezeeta.clone.backend.RestControllers;


import com.vezeeta.clone.backend.entities.Reservation;
import com.vezeeta.clone.backend.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

    @Autowired
    ReservationService reservationService;

    @PostMapping("")
    public ResponseEntity<Reservation> addReservation(@RequestBody Reservation reservation){
        try {
            reservation = this.reservationService.addReservation(reservation);
            System.out.println("Reservation is : " +  reservation); // after deserialization
        }catch(DataIntegrityViolationException exc){
            throw new DataIntegrityViolationException("Appointment is not Available");
        }
        /**
         * you might want to make the response follow the Hateoas  rest architecture.
         */
        return ResponseEntity.status(201).body(reservation);
    }

}

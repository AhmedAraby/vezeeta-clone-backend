package com.vezeeta.clone.backend.RestControllers;


import com.vezeeta.clone.backend.dao.clinicSchedule.ClinicScheduleDao;
import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.services.ClinicScheduleService;
import com.vezeeta.clone.backend.validation.constraints.WeekDayConstraint;
import org.apache.coyote.Response;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

/**
 * this controller need to be refactored
 * 1- the routes naming
 * 2- nested routes
 */

@RestController
@RequestMapping("/clinics")
@Validated  // to validate method parameters of this class
public class ClinicScheduleController {

    @Autowired
    private ClinicScheduleService clinicScheduleService;

    @GetMapping("{clinicId}/schedules")
    public ResponseEntity<List<ClinicSchedule>> getClinicSchedules(
            @PathVariable("clinicId") @Positive int clinicId)
    {
        List<ClinicSchedule> clinicSchedules = this.clinicScheduleService.getClinicSchedulesByWeekDay(clinicId);
        return ResponseEntity.ok(clinicSchedules);
    }

    @PostMapping("{clinicId}/schedules")
    public ResponseEntity<ClinicSchedule> addClinicSchedule(@Valid @RequestBody ClinicSchedule clinicSchedule)
    {
        clinicSchedule.setId(0);
        this.clinicScheduleService.addClinicSchedule(clinicSchedule);
        return ResponseEntity.status(201).body(clinicSchedule);
    }

    @PutMapping("{clinicId}/schedules")
    public ResponseEntity<Void> updateClinicSchedule(@Valid @RequestBody ClinicSchedule clinicSchedule)
    {
        this.clinicScheduleService.updateClinicSchedule(clinicSchedule);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("{clinicId}/schedules/{scheduleId}")
    public ResponseEntity<Void> deleteClinicSchedule(
            @PathVariable("scheduleId") @Positive int scheduleId)
    {
        this.clinicScheduleService.deleteClinicSchedule(scheduleId);
        return ResponseEntity.ok(null);
    }
}

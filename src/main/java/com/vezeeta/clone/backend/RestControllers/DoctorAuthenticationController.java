package com.vezeeta.clone.backend.RestControllers;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.login.LoginRequestModel;
import com.vezeeta.clone.backend.models.login.LoginSuccessResponseModel;
import com.vezeeta.clone.backend.models.signup.SignupSuccessResponseModel;
import com.vezeeta.clone.backend.security.models.DoctorDetails;
import com.vezeeta.clone.backend.security.tokens.DoctorUsernamePasswordAuthenticationToken;
import com.vezeeta.clone.backend.security.services.DoctorDetailsService;
import com.vezeeta.clone.backend.services.DoctorAuthenticationService;
import com.vezeeta.clone.backend.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DoctorAuthenticationController {

    @Autowired
    private DoctorDetailsService doctorDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DoctorAuthenticationService doctorAuthenticationService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/authenticate/login/doctor")
    public ResponseEntity<?> doctorLogin(@RequestBody LoginRequestModel loginRequest) {
        System.out.println("/authenticate/login/doctor");
        authenticationManager.authenticate( // throw error in case of failure
                new DoctorUsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(), loginRequest.getPassword()));
        DoctorDetails doctorDetails =
                (DoctorDetails) doctorDetailsService.loadUserByUsername(loginRequest.getEmail());
        // what is the possible Exceptions that could be thrown by JWT !!???
        Map<String, Object> claims = getClaims(doctorDetails);
        String jwt = jwtUtil.generateJWT(claims);
        return ResponseEntity.ok(new LoginSuccessResponseModel(doctorDetails.getId(), jwt));  // this was a bug
    }

    @PostMapping("/authenticate/signup/doctor")
    public ResponseEntity<SignupSuccessResponseModel> doctorSignUp(@RequestBody Doctor doctor) throws Exception {
        System.out.println("Controller: Doctor SignUp");
        doctor.setId(0);

        doctor = doctorAuthenticationService.SignUp(doctor);
        return ResponseEntity
                .status(201)
                .body(new SignupSuccessResponseModel(doctor.getId()));
    }
    private Map<String, Object> getClaims(DoctorDetails doctorDetails){
        Map<String, Object> claims = new HashMap<>();
        claims.put("email", doctorDetails.getUsername());
        claims.put("id", doctorDetails.getId());
        claims.put("name", doctorDetails.getName());
        claims.put("roles", doctorDetails.getRoles());

        return claims;
    }

}
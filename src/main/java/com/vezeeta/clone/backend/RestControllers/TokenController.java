package com.vezeeta.clone.backend.RestControllers;


import com.vezeeta.clone.backend.exceptions.InvalidUserTypeException;
import com.vezeeta.clone.backend.services.ClientAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    @Autowired
    private ClientAuthenticationService clientAuthenticationService;

    @GetMapping("/token/confirm")
    public String confirmToken(@RequestParam("token") String token,
                               @RequestParam("user_type") String userType) throws Exception
    {
        if(userType.equals("client")) {
            clientAuthenticationService.confirmToken(token);
        }
        else if(userType.equals("doctor")){
            // throw exception
        }
        else {
            String errorMsg = String.format("user type: %s is not valid, only client/doctor are valid users", userType);
            throw new InvalidUserTypeException(errorMsg);
        }

        return "confirmed"; // should redirect for login
    }
}

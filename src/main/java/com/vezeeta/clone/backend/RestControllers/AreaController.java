package com.vezeeta.clone.backend.RestControllers;

import com.vezeeta.clone.backend.dao.AreaDaoImpl;
import com.vezeeta.clone.backend.entities.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AreaController {
    @Autowired
    AreaDaoImpl areaDaoImpl;

    @GetMapping("/areas")
    public ResponseEntity<List<Area>> getAreas(){
        List<Area>  res = this.areaDaoImpl.getAreas();
        return ResponseEntity.ok(res);
    }
}

package com.vezeeta.clone.backend.security.tokens;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * the purpose of this class is assect the AuthenticationProvider to differentiate between
 * different types of users to select the right table to query.
 */
public class DoctorUsernamePasswordAuthenticationToken
        extends UsernamePasswordAuthenticationToken {
    public DoctorUsernamePasswordAuthenticationToken(Object prinsible, Object credentails){
        super(prinsible, credentails);
    }
}

/**
 * this class will retrieve the user from the database table client
 */
package com.vezeeta.clone.backend.security.services;


import com.vezeeta.clone.backend.entities.Client;
import com.vezeeta.clone.backend.security.models.ClientDetails;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class ClientDetailsService implements UserDetailsService {

    @Autowired
    private EntityManager entityManager; // this one will manage transactions by it self

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            Session session = entityManager.unwrap(Session.class);
            Query<Client> clientQ = session.createQuery("from Client where email=:email", Client.class);
            clientQ.setParameter("email", email);
            Client client = clientQ.getSingleResult(); // will throw exception if it could not found a record
            ClientDetails clientDetails = new ClientDetails(client.getId(), client.getName(),
                    client.getEmail(), client.isEnabled(),client.getPassword(), client.getRoles());
            return clientDetails;
        }
        catch(Exception exc){
            throw new UsernameNotFoundException("client email not found  : " + email);
        }
    }
}

package com.vezeeta.clone.backend.security.filters;

import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtExceptionHandlerFilter extends OncePerRequestFilter {
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain)
    throws ServletException, IOException
    {

        try{
            System.out.println("in ExceptionRedirectionResolverFilter");
            filterChain.doFilter(request, response);
            System.out.println("out of ExceptionRedirectionResolverFilter");

        }
        catch (SignatureException exc){
            System.out.println("inside ExceptionRedirectionFilter,  exception message is : " + exc.getMessage());
            System.out.println("inside ExceptionRedirectionFilter,  exception className is : " + exc.getClass());
            resolver.resolveException(request, response, null, exc);
        }
        catch(Exception exc){
            System.out.println("inside ExceptionRedirectionFilter,  exception message is : " + exc.getMessage());
            System.out.println("inside ExceptionRedirectionFilter,  exception className is : " + exc.getClass());
            resolver.resolveException(request, response, null, exc);
        }
    }
}

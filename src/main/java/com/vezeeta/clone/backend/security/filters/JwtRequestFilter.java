package com.vezeeta.clone.backend.security.filters;

import com.vezeeta.clone.backend.exceptions.InvalidAuthorizationHeader;
import com.vezeeta.clone.backend.exceptions.MissingAuthorizationHeaderException;
import com.vezeeta.clone.backend.security.services.ClientDetailsService;
import com.vezeeta.clone.backend.security.services.DoctorDetailsService;
import com.vezeeta.clone.backend.utils.JwtUtil;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 * extending 'OncePerRequestFilter' filter and annotating the filter
 * with @Component means it will run in the security filter chain e
 * event if we did not register it in the configuration,
 * the configuration register is just to define the position / order it will run at.
 */

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private DoctorDetailsService doctorDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
    throws ServletException, IOException
    {

        String uri = request.getRequestURI();
        boolean isPublicUri = this.isPublicUri(uri);
        if(isPublicUri){
            filterChain.doFilter(request, response);
            return ;
        }

        String authorizationHeader = request.getHeader("Authorization");
        if(authorizationHeader == null) {
            System.out.println("no authorization header");
            throw new MissingAuthorizationHeaderException("Authorization data is missed");
        }

        String [] parts = authorizationHeader.split(" ");
        if(parts.length !=2 || parts[0].equals("Bearer")==false) {
            System.out.println("Invalid Authorization Header");
            throw new InvalidAuthorizationHeader("Authorization header is not valid, supported Authorization Header should be of type 'Bearer'");
        }

        try{
            String jwt = parts[1];
            jwtUtil.verifyJWS(jwt);
            String roles = (String) jwtUtil.getCustomClaim("roles");
            List<GrantedAuthority> authorities = (List<GrantedAuthority>) getAuthorities(roles);
            String email = (String) jwtUtil.getCustomClaim("email");

            UserDetails principal;
            if(authorities.get(0).getAuthority().equals("ROLE_CLIENT"))
                principal = clientDetailsService.loadUserByUsername(email);
            else
                principal = doctorDetailsService.loadUserByUsername(email);

            UsernamePasswordAuthenticationToken authToken =
                    new UsernamePasswordAuthenticationToken(principal, null, authorities); // authorities for hasRole() and hasAuthorites()
            authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request)); // what is this for !!???

            // this line that make the user authenticated and provide
            SecurityContextHolder.getContext().setAuthentication(authToken);

            filterChain.doFilter(request, response);
            return ;
        }

        catch(SignatureException exc){
              throw exc;
//            throw new ServletException(exc.getMessage(), exc);
        }
        catch(Exception exc){
//            throw exc;
            throw new ServletException(exc.getMessage(), exc);  // we need to change this exception to type that we are able to handle probably
        }
    }


    private boolean isPublicUri(String uri)
    {
        String[] publicUris = {
                "^/token/.*",
                "^/authenticate/.*"
        };

        for(String publicUri : publicUris){
            Pattern pattern = Pattern.compile(publicUri);
            boolean match = pattern.matcher(uri).matches();
            if(match) return true;
        }
        return false;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String[] rolesArr = roles.split(",");
        for(int index=0; index<rolesArr.length; index++){
            authorities.add(new SimpleGrantedAuthority(rolesArr[index])); // I guess this object able to convert  ...
            // ROLE_ADMIN to ADMIN !!!
        }
        return authorities;
    }
}


/**
 * this filter will run after the spring builtin ExceptionTranslationFilter
 * to catch the AccessDeniedException before this filter, other wise ExceptionTranslationFilter
 * will handle the filter
 */

package com.vezeeta.clone.backend.security.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class AccessDeniedHandlerFiler extends OncePerRequestFilter {
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain)

    throws ServletException, IOException
    {
        try{
            filterChain.doFilter(request, response);
        }
        catch(AccessDeniedException exc){
            System.out.println("inside AccessDeniedHandlerFiler,  exception message is : " + exc.getMessage());
            System.out.println("inside AccessDeniedHandlerFiler,  exception className is : " + exc.getClass());
            resolver.resolveException(request, response, null, exc);
        }
        catch(Exception exc){
            System.out.println("inside AccessDeniedHandlerFiler,  exception message is : " + exc.getMessage());
            System.out.println("inside AccessDeniedHandlerFiler,  exception className is : " + exc.getClass());
            resolver.resolveException(request, response, null, exc);

        }

    }
}

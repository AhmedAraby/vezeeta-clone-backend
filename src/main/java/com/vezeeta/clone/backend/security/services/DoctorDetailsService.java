package com.vezeeta.clone.backend.security.services;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.security.models.DoctorDetails;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class DoctorDetailsService implements UserDetailsService {

    @Autowired
    private EntityManager entityManager;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            Session session = entityManager.unwrap(Session.class);
            Query<Doctor> doctorQ = session.createQuery("from Doctor where email=:email", Doctor.class);
            doctorQ.setParameter("email", email);
            Doctor doctor = doctorQ.getSingleResult();
            DoctorDetails doctorDetails = new DoctorDetails(doctor.getId(), doctor.getName(),
                    doctor.getEmail(), doctor.isEnabled(),doctor.getPassword(), doctor.getRoles());
            return doctorDetails;
        }
        catch(Exception exc){
            throw new UsernameNotFoundException("doctor email not found : " + email);
        }
    }
}

package com.vezeeta.clone.backend.security.tokens;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 * the purpose of this class is assect the AuthenticationProvider to differentiate between
 * different types of users to select the right table to query.
 */
public class ClientUsernamePasswordAuthenticationToken
        extends UsernamePasswordAuthenticationToken {
    public ClientUsernamePasswordAuthenticationToken(Object prinsible, Object credentails){
        super(prinsible, credentails);
    }
}

package com.vezeeta.clone.backend.security;


import com.vezeeta.clone.backend.exceptions.CustomAccessDeniedExceptionHandler;
import com.vezeeta.clone.backend.security.filters.AccessDeniedHandlerFiler;
import com.vezeeta.clone.backend.security.filters.CorsFilter;
import com.vezeeta.clone.backend.security.filters.JwtExceptionHandlerFilter;
import com.vezeeta.clone.backend.security.filters.JwtRequestFilter;
import com.vezeeta.clone.backend.security.providers.ClientAuthenticationProvider;
import com.vezeeta.clone.backend.security.providers.DoctorAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.session.SessionManagementFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private ClientAuthenticationProvider clientAuthenticationProvider;

    @Autowired
    private DoctorAuthenticationProvider doctorAuthenticationProvider;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    private AccessDeniedHandlerFiler accessDeniedHandlerFiler;
    @Autowired
    private JwtExceptionHandlerFilter jwtExceptionHandlerFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(clientAuthenticationProvider)
            .authenticationProvider(doctorAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // filters
        http.addFilterBefore(getCorsFilter(), ChannelProcessingFilter.class);  // first filter to run ever
        http.addFilterAfter(jwtExceptionHandlerFilter, CorsFilter.class);
        http.addFilterAfter(accessDeniedHandlerFiler, ExceptionTranslationFilter.class);
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);  // why do we put it before this filter !!??

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // no session as we use JWT
        http.csrf().disable();

        // configure routes
        http
                .authorizeRequests()
                .antMatchers("/authenticate/**").permitAll()
                .antMatchers("/token/**").permitAll()
                // all other routes require authentication
                .anyRequest().authenticated();
     }

    @Bean
    public CustomAccessDeniedExceptionHandler getCustomAccessDeniedExceptionHandler(){
        return new CustomAccessDeniedExceptionHandler();
    }

    // register AuthenticationManger as bean in spring container
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Bean
    public CorsFilter getCorsFilter(){
        // enable CORS policy for my frontEnd
        return new CorsFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        /**
         * I will use this password Encoder in the register process
         * to encode user password before saving the password to the DB.
         * and in authentication process to encode password before checking for match
         */
        return new BCryptPasswordEncoder();
    }
}

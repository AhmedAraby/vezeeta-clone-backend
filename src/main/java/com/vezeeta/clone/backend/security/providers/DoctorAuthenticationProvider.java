package com.vezeeta.clone.backend.security.providers;

import com.vezeeta.clone.backend.security.tokens.DoctorUsernamePasswordAuthenticationToken;
import com.vezeeta.clone.backend.security.models.DoctorDetails;
import com.vezeeta.clone.backend.security.services.DoctorDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class DoctorAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private DoctorDetailsService doctorDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        System.out.println("doctorAuthProvider,    email " + email  + "  " + "password " + password);
        DoctorDetails doctorDetails = null;
        try{doctorDetails = (DoctorDetails) doctorDetailsService.loadUserByUsername(email);}
        catch(UsernameNotFoundException ex){throw new BadCredentialsException("Invalid username/Password"); }

        if(authenticUser(doctorDetails.getPassword(),password))
            return new DoctorUsernamePasswordAuthenticationToken(email, password);
        else
            throw new BadCredentialsException("Invalid username/Password");  // is this the right behaviour for authProvider !!??
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(DoctorUsernamePasswordAuthenticationToken.class);
    }

    private boolean authenticUser(String DataBasePassword, String RequestPassword){
        return this.passwordEncoder.matches(RequestPassword, DataBasePassword);
    }
}

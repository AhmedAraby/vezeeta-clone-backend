package com.vezeeta.clone.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;

@Entity
@Table(name="clinic")
public class Clinic {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // identity will use the DB auto increment
	@Column(name="id")
	@PositiveOrZero(message = "id must not be negative")
	private int id;
	
	@Column(name="name")
	@NotBlank
	private String name;
	
	@Column(name="location")
	@NotBlank
	private String location;
	
	@Column(name="address")
	@NotBlank
	private String address; // detailed address (area/street/number/building/floor/apartment).

	@Column(name = "image_url")
	private String imageUrl;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="doctor_id")
	@NotNull
	private Doctor doctor;

	@Transient
	private int doctorId;
	
	@OneToMany(
				mappedBy="clinic",
				fetch=FetchType.LAZY,
				cascade=CascadeType.ALL,
				orphanRemoval=true)
	private List<ClinicSchedule> clinicSchedules;

	@OneToMany(
			mappedBy="clinic",
			cascade=CascadeType.ALL,
			fetch=FetchType.LAZY,
			orphanRemoval=true) // on removing a specific reservation from the clinic remove the reservation
	private List<Reservation> reservations;
	
	public Clinic() {}

	public Clinic(String name, String location, String address, String imageUrl) {
		this.name = name;
		this.location = location;
		this.address = address;
		this.imageUrl = imageUrl;
	}

	public Clinic(String name, String location, String address) {
		super();
		this.name = name;
		this.location = location;
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@JsonIgnore
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	@JsonIgnore
	public List<ClinicSchedule> getClinicSchedule() {
		return clinicSchedules;
	}
	public void setClinicSchedule(List<ClinicSchedule> clinicSchedule) {
		this.clinicSchedules = clinicSchedule;
	}
	
	public void addClinicSchedule(ClinicSchedule clinicSchedule)
	{
		if(this.clinicSchedules == null) // if the clinic do not exist in the data base and not tracked by hibernate
			this.clinicSchedules = new ArrayList<>();
		this.clinicSchedules.add(clinicSchedule);
		clinicSchedule.setClinic(this);
	}
	
	public void removeClinicSchedule(ClinicSchedule clinicSchedule)
	{
		this.clinicSchedules.remove(clinicSchedule); // bug if the list is empty, hibernate will load it, if there is no data to load / remove it will be empty initiaized ArrayList
		clinicSchedule.setClinic(null);
	}

	@JsonIgnore
	public List<ClinicSchedule> getClinicSchedules() {
		return clinicSchedules;
	}
	public void setClinicSchedules(List<ClinicSchedule> clinicSchedules) {
		this.clinicSchedules = clinicSchedules;
	}
	@JsonIgnore
	public List<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	public void addReservation(Reservation reservation)
	{
		if(this.reservations == null)
			this.reservations = new ArrayList<>();
		
		this.reservations.add(reservation);
		reservation.setClinic(this);
	}
	
	public void removeReservation(Reservation reservation)
	{	
		this.reservations.remove(reservation); // potential is the clinic is new and not persisted to the DB before !!?? test it !!
		reservation.setClinic(null);
	}

	public int getDoctorId() {
		return this.doctor.getId();
	}

	public void setDoctorId(int doctorId) {
		this.doctor = new Doctor();
		this.doctor.setId(doctorId);
	}

	@Override
	public String toString() {
		return "Clinic [id=" + id + ", name=" + name + ", location=" + location + ", address=" + address + ", schedules=" + clinicSchedules
				+ ", reservations=" + reservations;
	}
	
}

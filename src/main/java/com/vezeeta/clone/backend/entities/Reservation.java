package com.vezeeta.clone.backend.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="reservation")
public class Reservation {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // identity will use the DB auto increment
	@Column(name="id")
	private int id;
	
	/* relation mapping */

	@ManyToOne()
	@JoinColumn(name="client_id")
	private Client client;
	
	@Transient
	private int clientId;

	@ManyToOne()
	@JoinColumn(name="clinic_id")
	private Clinic clinic;

	@Transient
	private int clinicId; // for http and Jackson

	//**********************************************************************
	@Column(name="start_at")
    @Temporal(TemporalType.TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "Egypt") // support the deserialization
	private Date startAt;
	
	@Column(name="end_at")
    @Temporal(TemporalType.TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "Egypt")
	private Date endAt;
	
	@Column(name="date")
    @Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Egypt")
	private Date date;

	public Reservation() {}
	public Reservation(Date startAt, Date endAt, Date date) {
		super();
		this.startAt = startAt;
		this.endAt = endAt;
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	// reason of using JsonIgnore.
	// prevent JackSon from serializing this property into http response
	// (it will be a lot of bytes to move over the network, also this will result in infinite function calls hence stackOverFlow).
	@JsonIgnore
	public Client getClient() {
		return client;
	}
	@JsonProperty  // allow Jackson to deserialize this property from the http request
	public void setClient(Client client) {
		this.client = client;
	}

	@JsonIgnore
	public Clinic getClinic() {
		return clinic;
	}
	@JsonProperty
	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	public Date getStartAt() {
		return startAt;
	}
	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}
	public Date getEndAt() {
		return endAt;
	}
	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public int getClientId() {
		return this.client.getId();
	}

	public void setClientId(int clientId) {
		this.client = new Client();
		this.client.setId(clientId);
	}

	public int getClinicId() {
		return this.clinic.getId();
	}

	public void setClinicId(int clinicId) {
		this.clinic = new Clinic();
		this.clinic.setId(clinicId);
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", startAt=" + startAt + ", endAt=" + endAt + ", date=" + date + "]";
	}
}

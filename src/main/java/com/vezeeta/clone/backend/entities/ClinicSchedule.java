package com.vezeeta.clone.backend.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vezeeta.clone.backend.validation.constraints.WeekDayConstraint;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name="clinic_schedule")
public class ClinicSchedule {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // identity will use the DB auto increment
	@Column(name="id")
	@PositiveOrZero
	private int id;
	
	@Column(name="weekday")
	@WeekDayConstraint
	private String weekday;
	
	@Column(name="open_from")
    @Temporal(TemporalType.TIME)     // what is the use of this !!??? = it define the mapping type (date, time, time stamp)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "Egypt")
	@NotNull
	private Date openFrom;
	
	@Column(name="close_at")
    @Temporal(TemporalType.TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "Egypt")
	@NotNull
	private Date closeAt;
	
	@Column(name="appointment_span")
	@Positive
	private int appointmentSpan;
	
	@ManyToOne()
	@JoinColumn(name="clinic_id")
	@NotNull
	private Clinic clinic;  // this field and recursive validation could produce a bug.

	@Transient
	private int clinicId;

	public ClinicSchedule() {}


	public ClinicSchedule(String weekday, Date openFrom, Date closeAt, int appointmentSpan) {
		super();
		this.weekday = weekday;
		this.openFrom = openFrom;
		this.closeAt = closeAt;
		this.appointmentSpan = appointmentSpan;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getWeekday() {
		return weekday;
	}


	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}


	public Date getOpenFrom() {
		return openFrom;
	}


	public void setOpenFrom(Date openFrom) {
		this.openFrom = openFrom;
	}


	public Date getCloseAt() {
		return closeAt;
	}


	public void setCloseAt(Date closeAt) {
		this.closeAt = closeAt;
	}


	public int getAppointmentSpan() {
		return appointmentSpan;
	}


	public void setAppointmentSpan(int appointmentSpan) {
		this.appointmentSpan = appointmentSpan;
	}


	@JsonIgnore
	public Clinic getClinic() {
		return clinic;
	}


	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	public int getClinicId() {
		return this.clinic.getId();
	}

	public void setClinicId(int clinicId) {
		this.clinic = new Clinic();
		this.clinic.setId(clinicId);
	}

	@Override
	public String toString() {
		return "ClinicSchedule [id=" + id + ", weekday=" + weekday + ", openFrom=" + openFrom + ", closeAt=" + closeAt
				+ ", appointmentSpan=" + appointmentSpan + "]";
	}
	
}

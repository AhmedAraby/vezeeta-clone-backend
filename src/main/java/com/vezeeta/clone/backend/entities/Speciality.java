package com.vezeeta.clone.backend.entities;

import javax.persistence.*;

@Entity
@Table(name = "speciality")
public class Speciality {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public Speciality() {
    }

    public Speciality(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Area{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

package com.vezeeta.clone.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="client")
public class Client {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // identity will use the DB auto increment
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="email")
	private String email;

	@Column(name="password")
	private String password;

	@Column(name="phone")
	private String phone; // DB varchar type do not store 0 as prefix

	@Column(name="enabled")
	private boolean enabled;


	@Column(name="roles")
	private String roles;

	@OneToMany(
			mappedBy="client",
			cascade=CascadeType.ALL,
			fetch=FetchType.LAZY,
			orphanRemoval=true)
	private List<Reservation> reservations;

	@OneToOne(
			mappedBy="client",
			cascade=CascadeType.ALL,
			fetch=FetchType.LAZY,
			orphanRemoval=true)
	private ClientToken clientToken;

	public Client() {}

	public Client(String name, String email, String password, String phone, boolean enabled, String roles) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.enabled = enabled;
		this.roles = roles;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonIgnore
	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	public void addReservation(Reservation reservation)
	{
		if(this.reservations == null)
			this.reservations = new ArrayList<>();
		
		this.reservations.add(reservation);
		reservation.setClient(this);
	}
	
	public void removeReservation(Reservation reservation)
	{
//		would this be a bug if the client is already presisted !!???, test it
//		if(this.reservation == null)
//			this.reservation = new ArrayList<>();
		
		this.reservations.remove(reservation);
		reservation.setClient(null);
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@JsonIgnore
	public ClientToken getClientToken() {
		return clientToken;
	}

	public void setClientToken(ClientToken clientToken) {
		this.clientToken = clientToken;
	}


	@Override
	public String toString() {
		return "Client{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", phone='" + phone + '\'' +
				", enabled=" + enabled +
				", roles='" + roles + '\'' +
				", reservations=" + reservations +
				", clientToken=" + clientToken +
				'}';
	}
}

package com.vezeeta.clone.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vezeeta.clone.backend.validation.constraints.NewPasswordConstraint;
import com.vezeeta.clone.backend.validation.constraints.PasswordConstraint;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="doctor")

@NewPasswordConstraint // custom constraint for the newPassword and newPasswordConfirm fields
public class Doctor {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // identity will use the DB auto increment
	@Column(name="id")
	@PositiveOrZero(message = "doctor id must not be negative")
	private int id;
	
	@Column(name="name")
	@NotBlank
	private String name;
	
	@Column(name="email")
	@Email
	private String email;
	
	@Column(name="phone")
	@Pattern(regexp = "^0[0-9]{10}$", message = "phone number is not a valid Egyption numebr")
	private String phone; // DB varchar type do not store 0 as prefix
	
	@Column(name="speciality")
	@NotBlank
	private String speciality;  
	
	@Column(name="education")
	@NotBlank
	private String education; 
	
	@Column(name="fees")
	@Positive
	private int fees;
	
	@Column(name="photo_url")
	private String photoUrl; // link to S3 pucket

	@Column(name="password")
	@PasswordConstraint // custom composite constraint
	private String password;

	@Column(name="enabled")
	private boolean enabled;

	@JsonIgnore
	@Column(name="roles")
	private String roles;

	@OneToMany(
				fetch=FetchType.LAZY, 
				mappedBy="doctor", 
				cascade=CascadeType.ALL)
	private List<Clinic> clinics;

	// transient properties
	// for updating the password
	// is there any better practice
	@Transient
	private String newPassword;
	@Transient
	private String newPasswordConfirm;






	public Doctor() {}

	public Doctor(String name, String email, String phone, String speciality, String education,
				  int fees, String photoUrl, String password, boolean enabled, String roles) {
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.speciality = speciality;
		this.education = education;
		this.fees = fees;
		this.photoUrl = photoUrl;
		this.password = password;
		this.enabled = enabled;
		this.roles = roles;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getSpeciality() {
		return speciality;
	}


	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}


	public String getEducation() {
		return education;
	}


	public void setEducation(String education) {
		this.education = education;
	}


	public int getFees() {
		return fees;
	}


	public void setFees(int fees) {
		this.fees = fees;
	}


	public String getPhotoUrl() {
		return photoUrl;
	}


	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	@JsonIgnore
	public List<Clinic> getClinics() {
		return clinics;
	}
	public void setClinics(List<Clinic> clinics) {
		this.clinics = clinics;
	}

	public String getPassword() {
		return password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	// stress on theses methods 
	public void addClinic(Clinic clinic)
	{
		if(this.clinics == null) // in case doctor is new and not persisted to the DB before
			this.clinics = new ArrayList<>();
		this.clinics.add(clinic);
		clinic.setDoctor(this); // hibernate will not do this for me, I have to do it manually
	}
	
	public void removeClinic(Clinic clinic)
	{
		this.clinics.remove(clinic); // potential bug if the doctor is new
		clinic.setDoctor(null);
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@JsonIgnore  // disable both serialization and deserialization
	public String getNewPassword() {
		return newPassword;
	}
	@JsonProperty // enable deserialization
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@JsonIgnore
	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}
	@JsonProperty
	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}

	@Override
	public String toString() {
		return "Doctor{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				", speciality='" + speciality + '\'' +
				", education='" + education + '\'' +
				", fees=" + fees +
				", photoUrl='" + photoUrl + '\'' +
				", password='" + password + '\'' +
				", enabled=" + enabled +
				", roles='" + roles + '\'' +
				", clinics=" + clinics +
				", newPassword='" + newPassword + '\'' +
				", newPasswordConfirm='" + newPasswordConfirm + '\'' +
				'}';
	}
}

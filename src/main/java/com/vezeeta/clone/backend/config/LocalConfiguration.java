package com.vezeeta.clone.backend.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;


@Configuration
@ComponentScan

public class LocalConfiguration {

    @PostConstruct
    public void init(){
//        TimeZone.setDefault(TimeZone.getTimeZone("EET")); // it is already the default
        System.out.println("in init");
//        System.out.println("set time zone to EET");

    }
}

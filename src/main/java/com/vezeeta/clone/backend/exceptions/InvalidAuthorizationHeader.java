package com.vezeeta.clone.backend.exceptions;

public class InvalidAuthorizationHeader extends RuntimeException{
    public InvalidAuthorizationHeader() {
    }

    public InvalidAuthorizationHeader(String message) {
        super(message);
    }

    public InvalidAuthorizationHeader(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAuthorizationHeader(Throwable cause) {
        super(cause);
    }

    public InvalidAuthorizationHeader(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

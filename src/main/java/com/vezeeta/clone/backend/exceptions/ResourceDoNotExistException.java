package com.vezeeta.clone.backend.exceptions;

public class ResourceDoNotExistException extends RuntimeException {
    public ResourceDoNotExistException() {
    }

    public ResourceDoNotExistException(String message) {
        super(message);
    }

    public ResourceDoNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceDoNotExistException(Throwable cause) {
        super(cause);
    }

    public ResourceDoNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

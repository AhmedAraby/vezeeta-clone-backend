package com.vezeeta.clone.backend.exceptions;

import com.vezeeta.clone.backend.models.GeneralErrorResponseModel;
import com.vezeeta.clone.backend.models.signup.SignupErrorResponseModel;
import com.vezeeta.clone.backend.models.validation.ValidationErrorsResponseModel;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class CustomRestExceptionHandler {



    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleInvalidUserTypeException(InvalidUserTypeException ex, WebRequest wr){
        System.out.println("inside -- handleInvalidUserTypeException");
        return new ResponseEntity<GeneralErrorResponseModel>(new GeneralErrorResponseModel(ex.getMessage(), HttpStatus.BAD_REQUEST.value()), null, HttpStatus.BAD_REQUEST);
    }



    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleInvalidTokenException(InvalidTokenException ex, WebRequest wr){
        System.out.println("inside -- handleInvalidTokenException");
        return new ResponseEntity<GeneralErrorResponseModel>(new GeneralErrorResponseModel(ex.getMessage(),  HttpStatus.BAD_REQUEST.value()), null, HttpStatus.BAD_REQUEST);
    }

    // UnAuthorized requests
    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleBadCredentialsException(BadCredentialsException ex, WebRequest wr){
        System.out.println("inside -- handleBadCredentialsException");
        return new ResponseEntity<GeneralErrorResponseModel>(new GeneralErrorResponseModel(ex.getMessage(), HttpStatus.UNAUTHORIZED.value()), null, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleTokenExpiredException(TokenExpiredException ex, WebRequest wr){
        System.out.println("inside -- handleTokenExpiredException");
        // we should redirect the user to issue new token !!!
        return new ResponseEntity<GeneralErrorResponseModel>(new GeneralErrorResponseModel(ex.getMessage(), HttpStatus.UNAUTHORIZED.value()), null, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleSignatureException(SignatureException exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.UNAUTHORIZED.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleMissingAuthorizationHeaderException(MissingAuthorizationHeaderException exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.UNAUTHORIZED.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleInvalidAuthorizationHeader(InvalidAuthorizationHeader exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.UNAUTHORIZED.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleAccessDeniedException(AccessDeniedException ex, WebRequest wr){
        System.out.println("inside -- handleAccessDeniedException");
        return new ResponseEntity<GeneralErrorResponseModel>(new GeneralErrorResponseModel(ex.getMessage(), HttpStatus.FORBIDDEN.value()), null, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    public ResponseEntity<SignupErrorResponseModel> handleEmailAlreadyExistException(EmailAlreadyExistException ex, WebRequest wr){
        System.out.println("inside -- handleEmailAlreadyExistException");
        return new ResponseEntity<SignupErrorResponseModel>(
                new SignupErrorResponseModel(ex.getMessage(), ex.getErrorCode()),  // special error code not http status code.
                null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleInvalidUserInputException
            (InvalidUserInputException exc, WebRequest wr)
    {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleDataIntegrityViolationException
            (DataIntegrityViolationException exc, WebRequest wr)
    {
        /**
         * this exception is thrown on violating database constraints
         * - unique
         * - foreign key
         *
         * how to know the specific field that generated the problem
         */
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(new GeneralErrorResponseModel(exc.getMostSpecificCause().getMessage(), HttpStatus.CONFLICT.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleEmptyResultDataAccessException
            (EmptyResultDataAccessException exc, WebRequest wr)
    {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new GeneralErrorResponseModel("Resource do not Exist", HttpStatus.NOT_FOUND.value()));
    }


    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleResourceDoNotExistException
            (ResourceDoNotExistException exc, WebRequest wr)
    {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.NOT_FOUND.value()));
    }

    @ExceptionHandler
    public ResponseEntity<ValidationErrorsResponseModel> handleMethodArgumentNotValidException
            (MethodArgumentNotValidException exc, WebRequest wr)
    {
        List<ObjectError> errors = exc.getAllErrors();
        ValidationErrorsResponseModel validationErrorsResponse = new ValidationErrorsResponseModel();

        for(ObjectError error: errors){
            FieldError fError = (FieldError) error;
            validationErrorsResponse.addError(fError.getField(), fError.getDefaultMessage());
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationErrorsResponse);
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleConstraintViolationException
            (ConstraintViolationException exc, WebRequest wr)
    {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.BAD_REQUEST.value()));
    }



    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.METHOD_NOT_ALLOWED.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleHttpMessageNotReadableException(HttpMessageNotReadableException exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exc, WebRequest wr){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GeneralErrorResponseModel(exc.getMessage(), HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler
    public ResponseEntity<ValidationErrorsResponseModel> handleBindException(BindException exc, WebRequest wr){
        List<ObjectError> errors = exc.getAllErrors();
        ValidationErrorsResponseModel validationErrorsResponse = new ValidationErrorsResponseModel();

        for(ObjectError error: errors){
            FieldError fError = (FieldError) error;
            validationErrorsResponse.addError(fError.getField(), fError.getDefaultMessage());
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationErrorsResponse);
    }




    @ExceptionHandler
    public ResponseEntity<GeneralErrorResponseModel> handleAllRemainingExceptions(Exception ex, WebRequest wr){
        System.out.println("\n");
        System.out.println("inside -- catch all remaining Exception");
        System.out.println("exception class name " + ex.getClass().getName());
        System.out.println("exception message " + ex.getMessage());
        System.out.println("\n");

        return new ResponseEntity<GeneralErrorResponseModel>(
                new GeneralErrorResponseModel(ex.getMessage() + " ----- UnKnown exception", HttpStatus.INTERNAL_SERVER_ERROR.value()),
                null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
package com.vezeeta.clone.backend.exceptions;

public class MissingAuthorizationHeaderException extends RuntimeException{
    public MissingAuthorizationHeaderException() {
    }

    public MissingAuthorizationHeaderException(String message) {
        super(message);
    }

    public MissingAuthorizationHeaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingAuthorizationHeaderException(Throwable cause) {
        super(cause);
    }

    public MissingAuthorizationHeaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

/**
 * this exception will be thrown weather when the token expiration data is passed
 * or when the token is already used for confirmation.
 */
package com.vezeeta.clone.backend.exceptions;

public class TokenExpiredException extends RuntimeException{
    public TokenExpiredException() {
    }

    public TokenExpiredException(String message) {
        super(message);
    }

    public TokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenExpiredException(Throwable cause) {
        super(cause);
    }
}

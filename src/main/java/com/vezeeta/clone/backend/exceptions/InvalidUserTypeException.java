/**
 * the only supported user types are:
 * 1- Client
 * 2- Doctor
 * other wise this Exception will be thrown.
 */

package com.vezeeta.clone.backend.exceptions;
public class InvalidUserTypeException extends RuntimeException{
    public InvalidUserTypeException() {
    }

    public InvalidUserTypeException(String message) {
        super(message);
    }

    public InvalidUserTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUserTypeException(Throwable cause) {
        super(cause);
    }
}

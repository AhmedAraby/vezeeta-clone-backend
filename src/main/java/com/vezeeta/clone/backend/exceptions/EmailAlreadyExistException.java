/**
 * if user tried to signup with email that already exist int the database
 * and is enabled then the application should throw this exception.
 */
package com.vezeeta.clone.backend.exceptions;

public class EmailAlreadyExistException extends RuntimeException{

    /**
     * 1 = email exist and enabled, user another email
     * 2 = email exist but need to be confirmed, please confirm.
     */

    private int errorCode ;

    public EmailAlreadyExistException() {
    }

    public EmailAlreadyExistException(String message, int errorCode){
        super(message);
        this.errorCode = errorCode;
    }


    public EmailAlreadyExistException(String message, Throwable cause, int errorCode)
    {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}

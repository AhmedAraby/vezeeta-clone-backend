package com.vezeeta.clone.backend.utils;

// jjwt
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.stereotype.Component;

// java
import java.security.Key;
import java.util.Date;
import java.util.Map;

@Component
public class JwtUtil {
    private Claims claims=null;

    public JwtUtil(){}

    // generate key for specific algorithm, (best practice security) it all depends on the character count
    static private Key key=null;
        static Key  getSecureKey(){
        if(key !=null) return key;
        key = Keys.secretKeyFor(SignatureAlgorithm.HS256); // 32 byte = 32 character
        return key;
    }

    static int getExpirationPeriodMs(){
        return 1 * 60 * 1000; // 1 minute in MS
    }


    private Claims getClaims() throws Exception {
        if(claims !=null) return claims;
        throw new Exception("claims are not parsed/verified yet");
    }

    public String getSubject() throws Exception {
        getClaims();
        return claims.getSubject();
    }

    public Object getCustomClaim(String claimKey) throws Exception {
        getClaims();
        return claims.get(claimKey);
    }

    public String generateJWT(Map<String, Object> claims){
        return createJWS(claims, (String) claims.get("email"));
    }

    public String createJWS(Map<String, Object> claims, String subject){
            return Jwts.builder()
                    .setSubject(subject)
                    .setIssuer("vezeeta.clone.org")
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date((System.currentTimeMillis())))// + getExpirationPeriodMs())))
                    .setClaims(claims)
                    .signWith(getSecureKey())
                    .compact(); // build the jwt and sign it with the secret key
    }

    public Claims verifyJWS(String jws)
    {
        try {
            /**
             * would this check for expiration date also !!???
             */
            return claims = Jwts.parserBuilder()
                    .setSigningKey(getSecureKey())
                    .build() // get thread safe jws parser, what is the need for thread safe in this situation !!??
                    .parseClaimsJws(jws).getBody();
        }
        /*
        * I want to catch and throw expiration exception
        * and other fine grained exception
        *  !!!*/
        catch(SignatureException exc){
            System.out.println("****** invalid jwt : ");
            throw exc;
        }
        catch (JwtException exc){
            System.out.println("****** parsing failed");
            throw exc;
        }
    }


}

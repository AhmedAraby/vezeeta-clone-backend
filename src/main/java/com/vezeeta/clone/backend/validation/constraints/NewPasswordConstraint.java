package com.vezeeta.clone.backend.validation.constraints;

import com.vezeeta.clone.backend.validation.validators.NewPasswordConstraintValidator;
import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {NewPasswordConstraintValidator.class})

public @interface NewPasswordConstraint {

    String message() default "newPassword and newPasswordConfirm fields are invalid";

    // reflection stuff
    Class<?> [] groups() default {};  // assign empty array
    Class<? extends Payload> [] payload() default {}; // assign empty array

}

package com.vezeeta.clone.backend.validation.validators;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.validation.constraints.NewPasswordConstraint;
import com.vezeeta.clone.backend.validation.constraints.WeekDayConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class WeekDayConstraintValidator implements ConstraintValidator<WeekDayConstraint, String> {
    @Override
    public void initialize(WeekDayConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
    @Override
    public boolean isValid(String weekDay, ConstraintValidatorContext constraintValidatorContext) {
        String weekDays[] = {
                "sunday","monday",
                "tuesday","wednesday",
                "thursday","friday",
                "saturday"
        };

        for(String day : weekDays){
            if(day.equals(weekDay))
                return true;
        }
        return false;
    }
}

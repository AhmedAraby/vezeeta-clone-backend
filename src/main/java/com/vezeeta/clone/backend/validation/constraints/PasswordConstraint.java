/**
 * this is a composite constraint
 * it combines multiple built in constraints
 */
package com.vezeeta.clone.backend.validation.constraints;


import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

@Target({FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {}) // is this of any use in our situation here !!???

// javax constraints we need to apply
@Length(min = 10, message = "password length must be >= 10 characters")
@Pattern(regexp = "^.*[0-9]+.*$", message = "password should contain numbers")  // must have numbers
@Pattern(regexp = "^.*[a-z]+.*$", message = "password should contain lower case chars")  // must have small chars
@Pattern(regexp = "^.*[A-Z]+.*$", message = "password should contain capital chars")  // must have caps chars

public @interface PasswordConstraint {
    /**
     * how to get error messages from the builtin constraint at once  ???
     */

    // without theses fields spring will throw exceptions
    // all the bellow are just properties of this annotation
    String message() default "failed composite constraint";
    // reflection stuff
    Class<?> [] groups() default {};  // assign empty array
    Class<? extends Payload> [] payload() default {}; // assign empty array

}

package com.vezeeta.clone.backend.validation.constraints;

import com.vezeeta.clone.backend.validation.validators.NewPasswordConstraintValidator;
import com.vezeeta.clone.backend.validation.validators.WeekDayConstraintValidator;
import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {WeekDayConstraintValidator.class})

public @interface WeekDayConstraint {

    String message() default "invalid week day";

    // reflection stuff
    Class<?> [] groups() default {};  // assign empty array
    Class<? extends Payload> [] payload() default {}; // assign empty array

}

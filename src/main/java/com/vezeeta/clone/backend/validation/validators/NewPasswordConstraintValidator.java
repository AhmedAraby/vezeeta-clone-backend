package com.vezeeta.clone.backend.validation.validators;

import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.validation.constraints.NewPasswordConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class NewPasswordConstraintValidator implements ConstraintValidator<NewPasswordConstraint, Doctor> {
    @Override
    public void initialize(NewPasswordConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Doctor doctor, ConstraintValidatorContext constraintValidatorContext) {

        // for jpa validation before persisting, as merge will ignore theses fields, hence they will be null.
        if(doctor.getNewPassword() == null && doctor.getNewPasswordConfirm() == null)
            return true;

        if(doctor.getNewPassword() == ""
                && doctor.getNewPasswordConfirm() == "" )
            return true;
        else if(doctor.getNewPassword().length() < 10){
            this.buildConstraintViolation(
                    constraintValidatorContext,
                    "newPassword",
                    "newPassword length must be >= 10 character");
            return false;
        }


        Pattern passwordHasNumbersP = Pattern.compile("^.*[0-9]+.*$");
        boolean passwordHasNumbers = passwordHasNumbersP.matcher(doctor.getNewPassword()).matches();

        Pattern passwordHasLowerCharsP = Pattern.compile("^.*[a-z]+.*$");
        boolean passwordHasLowerChars = passwordHasLowerCharsP.matcher(doctor.getNewPassword()).matches();

        Pattern passwordHasCapsCharsP = Pattern.compile("^.*[A-Z]+.*$");
        boolean passwordHasCapsChars = passwordHasCapsCharsP.matcher(doctor.getNewPassword()).matches();

        if(!passwordHasNumbers){
            this.buildConstraintViolation(
                    constraintValidatorContext,
                    "newPassword",
                    "newPassword must contain numbers");
            return false;
        }
        else if(!passwordHasLowerChars){
            this.buildConstraintViolation(
                    constraintValidatorContext,
                    "newPassword",
                    "newPassword must have lower case alphabet characters");
            return false;
        }
        else if(!passwordHasCapsChars){
            this.buildConstraintViolation(
                    constraintValidatorContext,
                    "newPassword",
                    "newPassword must have capital case alphabet characters");
            return false;
        }

        else if(doctor.getNewPassword().equals(doctor.getNewPasswordConfirm()) == false)
        {
            this.buildConstraintViolation(
                    constraintValidatorContext,
                    "newPasswordConfirm",
                    "newPasswordConfirm must match newPassword field");
            return false;
        }
        return true;
    }

    private void buildConstraintViolation(
            ConstraintValidatorContext constraintValidatorContext, // reference
            String key, String message)
    {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext
                .buildConstraintViolationWithTemplate(message)
                .addPropertyNode(key)
                .addConstraintViolation();
        return ;
    }
}

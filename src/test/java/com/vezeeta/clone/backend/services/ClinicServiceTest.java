package com.vezeeta.clone.backend.services;

import com.vezeeta.clone.backend.dao.clinic.ClinicDao;
import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.ClinicSchedule;
import com.vezeeta.clone.backend.exceptions.ResourceDoNotExistException;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClinicServiceTest {

    @Mock
    ClinicDao clinicDaoImpl;

    @InjectMocks
    private ClinicService clinicService;

    @Test
    void getClinicTest() {
        // given
        Clinic clinic = new Clinic("clinic #1 ", "cairo", "down town", "");
        clinic.setDoctorId(1);
        clinic.setId(1);
        when(clinicDaoImpl.getClinic(1)).thenReturn(clinic);

        // when
        Clinic clinicRes = clinicService.getClinic(1);

        // then
        assertEquals(clinic, clinicRes);
        verify(clinicDaoImpl, times(1)).getClinic(1); // behaviour check
    }

    @Test
    void getClinicsResultSetEqualPageSizeTest()
    {
        // given
        int pageSize = 3;
        int doctorId = 1;
        String area = "cairo";
        // insert clinics
        List<Clinic> expectedClinic = new ArrayList<>();
        for(int index = 0; index<3; index++){
            Clinic clinic = new Clinic("", area, "", "");
            clinic.setId(index+1);
            clinic.setDoctorId(doctorId);
            expectedClinic.add(clinic);

        }
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(doctorId, area);
        PaginationInfoModel paginationInfoModel = new PaginationInfoModel(1, pageSize);
        // configure the mock
        when(clinicDaoImpl.getClinics(clinicSearchParamsModel, paginationInfoModel)).thenReturn(expectedClinic);


        // when
        List<Clinic> clinicRes = clinicService.getClinics(clinicSearchParamsModel, paginationInfoModel);

        // then
        // verify result
        assertEquals(clinicRes.size() , pageSize);
        for(int index=0; index< clinicRes.size(); index ++){
            assertEquals(clinicRes.get(index).getDoctorId(), doctorId);
            assertEquals(clinicRes.get(index).getLocation(), area);
        }
        // verify the input and behaviour
        verify(clinicDaoImpl, times(1)).getClinics(clinicSearchParamsModel, paginationInfoModel);
    }

    @Test
    @Disabled
    void addClinicTest() {

        // no need to test
    }

    @Test
    void updateClinicAndClinicExistTest() {
        // given
        Clinic clinic = new Clinic("", "", "", "");
        clinic.setId(1);
        clinic.setDoctorId(1);
        when(clinicDaoImpl.clinicExist(1, 1)).thenReturn(true);

        // when
        clinicService.updateClinic(clinic);

        // then
        verify(clinicDaoImpl, times(1)).clinicExist(1, 1);
        verify(clinicDaoImpl, times(1)).update(clinic);
    }


    @Test
    void updateClinicAndClinicDoNotExistTest() {
        // given
        Clinic clinic = new Clinic("", "", "", "");
        clinic.setId(1);
        clinic.setDoctorId(1);
        when(clinicDaoImpl.clinicExist(1, 1)).thenReturn(false);

        // when
        // then
        assertThrows(ResourceDoNotExistException.class, ()->{
            clinicService.updateClinic(clinic);
        });
        verify(clinicDaoImpl, times(1)).clinicExist(1, 1);
    }

    @Test
    void getClinicCountAreaExistTest() {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(1, "cairo");
        when(clinicDaoImpl.getClinicsCount(clinicSearchParamsModel)).thenReturn(3L);

        // when
        long countRes = clinicService.getClinicCount(clinicSearchParamsModel);

        // then
        assertEquals(countRes, 3L);
        verify(clinicDaoImpl, times(1)).getClinicsCount(clinicSearchParamsModel);
    }

    @Test
    void getClinicCountAreaDoNotExistTest() {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(1, null);
        when(clinicDaoImpl.getClinicsCount(clinicSearchParamsModel)).thenReturn(10L);

        // when
        long countRes = clinicService.getClinicCount(clinicSearchParamsModel);

        // then
        assertEquals(countRes, 10L);
        verify(clinicDaoImpl, times(1)).getClinicsCount(clinicSearchParamsModel);
    }
    @Test
    void getClinicSchedule() {

        // given
        String weekDay = "friday";
        int clinicId = 1;
        ClinicSchedule expectedClinicSchedule = new ClinicSchedule(weekDay, null, null, 20);
        expectedClinicSchedule.setClinicId(clinicId);
        when(clinicDaoImpl.getClinicSchedule(clinicId, weekDay)).thenReturn(expectedClinicSchedule);

        // when
        ClinicSchedule clinicScheduleRes = clinicService.getClinicSchedule(clinicId, weekDay);

        // then
        assertEquals(expectedClinicSchedule, clinicScheduleRes);
        verify(clinicDaoImpl, times(1)).getClinicSchedule(clinicId, weekDay);
    }

    @Test
    void getClinicReservations() {
    }
}
package com.vezeeta.clone.backend.dao.clinic;

import com.vezeeta.clone.backend.dao.doctor.resource.DoctorDaoImpl;
import com.vezeeta.clone.backend.entities.Clinic;
import com.vezeeta.clone.backend.entities.Doctor;
import com.vezeeta.clone.backend.models.PaginationInfoModel;
import com.vezeeta.clone.backend.models.clinic.ClinicSearchParamsModel;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ClinicDaoImplTest {

    @Autowired
    private EntityManager entityManager;
    private ClinicDaoImpl clinicDao;

    @BeforeEach
    void setup(){
        clinicDao = new ClinicDaoImpl();
        clinicDao.setEntityManager(entityManager);
    }

    @Test
    void shouldAddClinicTest()
    {
        // given
        Doctor doctor = new Doctor(
                "abdo", "abdo@gmail.com",
                "01068482084", "dentist",
                "mti", 200, "",
                "0173706505sS", true,
                "ROLE_DOCTOR");
        DoctorDaoImpl doctorDao = new DoctorDaoImpl();
        doctorDao.setEntityManager(entityManager);
        doctorDao.add(doctor);

        Clinic clinic = new Clinic("clinic1", "cairo", "cairo", "");
        int doctorId = doctor.getId();
        clinic.setDoctorId(doctorId);

        // when
        clinicDao.add(clinic);

        // then
        assertTrue(clinic.getId() !=0);
    }

    @Test
    void shouldFailToAddClinicAttachedToNonExistDoctorTest()
    {
        // given
        Clinic clinic = new Clinic("clinic2", "cairo", "cairo", "");
        int doctorId = 1;
        clinic.setDoctorId(doctorId);

        // when
        // then
        assertThrows(ConstraintViolationException.class, ()-> clinicDao.add(clinic));
    }

    @Test
    void getQueryConditionsAllParamsExistTest()
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(1, "cairo");

        // when
        String conditions = clinicDao.getQueryConditions(clinicSearchParamsModel);

        // then
        assertTrue(conditions.contains("where"), "where is missed");
        assertTrue(conditions.contains("and"), "and is missed");
        assertTrue(conditions.contains("d.id=:doctorId"), "doctorId is missed");
        assertTrue(conditions.contains("c.location=:area"), "area is missed");
    }

    @Test
    void getQueryConditionsOnlyDoctorIdExistTest()
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(1, null);

        // when
        String conditions = clinicDao.getQueryConditions(clinicSearchParamsModel);

        // then
        assertTrue(conditions.contains("where"), "where is missed");
        assertTrue(conditions.contains("d.id=:doctorId"), "doctorId is missed");


        assertFalse(conditions.contains("and"), "and should not exist");
        assertFalse(conditions.contains("c.location=:area"), "area should not exist");
    }


    @Test
    void getQueryConditionsOnlyAreaExistTest()
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(0, "cairo");

        // when
        String conditions = clinicDao.getQueryConditions(clinicSearchParamsModel);

        // then
        assertTrue(conditions.contains("where"), "where is missed");
        assertTrue(conditions.contains("c.location=:area"), "area is missed");

        assertFalse(conditions.contains("and"), "and should not exist");
        assertFalse(conditions.contains("d.id=:doctorId"), "doctorId should not exist ");
    }

    @ParameterizedTest
    @MethodSource(value = "getClinicSearchParams")
    void setQueryCondsParametersAllSearchParamsExistTest(int doctorId, String area)
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(doctorId, area);
        String queryStr = " select c from Clinic c " +
                " join c.doctor d " +
                " where d.id=:doctorId " +
                " and c.location=:area " +
                " order by c.id ";
        Session session = entityManager.unwrap(Session.class);

        // when
        Query<Clinic> clinicQuery = session.createQuery(queryStr, Clinic.class);
        clinicQuery = clinicDao.setQueryCondsParameters(clinicQuery, clinicSearchParamsModel);

        assertEquals(clinicQuery.getParameterValue("doctorId"), doctorId);
        assertEquals(clinicQuery.getParameterValue("area"), area);
    }

    @ParameterizedTest
    @MethodSource(value = "getClinicSearchParams")
    void setQueryCondsParametersOnlyDoctorIdExistsTest(int doctorId, String area)
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(doctorId, null);
        String queryStr = " select c from Clinic c " +
                " join c.doctor d " +
                " where d.id=:doctorId " +
                " order by c.id ";
        Session session = entityManager.unwrap(Session.class);

        // when
        Query<Clinic> clinicQuery = session.createQuery(queryStr, Clinic.class);
        clinicQuery = clinicDao.setQueryCondsParameters(clinicQuery, clinicSearchParamsModel);

        assertEquals(clinicQuery.getParameterValue("doctorId"), doctorId);
    }

    @ParameterizedTest
    @MethodSource(value = "getClinicSearchParams")
    void setQueryCondsParametersOnlyAreaExistsTest(int doctorId, String area)
    {
        // given
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(0, area);
        String queryStr = " select c from Clinic c " +
                " join c.doctor d " +
                " where c.location=:area " +
                " order by c.id ";
        Session session = entityManager.unwrap(Session.class);

        // when
        Query<Clinic> clinicQuery = session.createQuery(queryStr, Clinic.class);
        clinicQuery = clinicDao.setQueryCondsParameters(clinicQuery, clinicSearchParamsModel);

        assertEquals(clinicQuery.getParameterValue("area"), area);
    }

    @ParameterizedTest
    @MethodSource(value = "paginationInfo")
    void getClinicAllSearchParamsExistAndResultCountEqualPageSizeTest(PaginationInfoModel paginationInfoModel)
    {
        // given
        // add doctor for refrential integrity
        Doctor doctor = new Doctor(
                "abdo", "abdo@gmail.com",
                "01068482084", "dentist",
                "mti", 200, "",
                "0173706505sS", true,
                "ROLE_DOCTOR");
        DoctorDaoImpl doctorDao = new DoctorDaoImpl();
        doctorDao.setEntityManager(entityManager);
        doctor.setId(1);
        doctorDao.add(doctor);

        Clinic clinic = new Clinic("clinic - " + 1, "alex", "alex", "");
        clinic.setDoctorId(doctor.getId());
        clinic.setId(1); // be explicit for testing
        clinicDao.add(clinic);

        for(int index = 1; index < 6; index ++){
            clinic = new Clinic("clinic - " + index, "cairo", "cairo", "");
            clinic.setDoctorId(doctor.getId());
            clinic.setId(index + 1); // be explicit for testing
            clinicDao.add(clinic);
        }

        int pageSize = 2;
        ClinicSearchParamsModel clinicSearchParamsModel = new ClinicSearchParamsModel(doctor.getId(), "cairo");

        // when
        List<Clinic> resClinics = clinicDao.getClinics(clinicSearchParamsModel, paginationInfoModel);

        // then
        assertEquals(resClinics.size(), paginationInfoModel.getPageSize(), "there is no enough clinics for the page size");
        for(int index=0; index < resClinics.size(); index ++){
            assertEquals(resClinics.get(index).getLocation(), "cairo");
        }
    }


    private static String[][] getClinicSearchParams(){
        String[][] params = {
                {"1", "cairo"},
                {"1", "giza"},
                {"2", "cairo"}
        };

        return params;
    }
    private static List<PaginationInfoModel> paginationInfo()
    {
        List<PaginationInfoModel> paginationInfoModels = new ArrayList<>();
        paginationInfoModels.add(new PaginationInfoModel(1, 1));
        paginationInfoModels.add(new PaginationInfoModel(1, 2));
        paginationInfoModels.add(new PaginationInfoModel(2, 1));
        paginationInfoModels.add(new PaginationInfoModel(2, 2));
        paginationInfoModels.add(new PaginationInfoModel(1, 5));

        return paginationInfoModels;
    }



}